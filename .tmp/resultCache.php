<?php declare(strict_types = 1);

return [
	'lastFullAnalysisTime' => 1633093015,
	'meta' => array (
  'cacheVersion' => 'v9-project-extensions',
  'phpstanVersion' => '0.12.99',
  'phpVersion' => 80011,
  'projectConfig' => '{parameters: {level: max, paths: [/var/www/html/tests], tmpDir: /var/www/html/.tmp}}',
  'analysedPaths' => 
  array (
    0 => '/var/www/html/tests',
  ),
  'scannedFiles' => 
  array (
  ),
  'composerLocks' => 
  array (
    '/var/www/html/composer.lock' => '0335c269324deda97929db45f96b25b9943a096a',
  ),
  'composerInstalled' => 
  array (
    '/var/www/html/vendor/composer/installed.php' => 
    array (
      'versions' => 
      array (
        'brick/math' => 
        array (
          'pretty_version' => '0.9.3',
          'version' => '0.9.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../brick/math',
          'aliases' => 
          array (
          ),
          'reference' => 'ca57d18f028f84f777b2168cd1911b0dee2343ae',
          'dev_requirement' => false,
        ),
        'cordoval/hamcrest-php' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '*',
          ),
        ),
        'davedevelopment/hamcrest-php' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '*',
          ),
        ),
        'doctrine/inflector' => 
        array (
          'pretty_version' => '2.0.3',
          'version' => '2.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../doctrine/inflector',
          'aliases' => 
          array (
          ),
          'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
          'dev_requirement' => false,
        ),
        'doctrine/instantiator' => 
        array (
          'pretty_version' => '1.4.0',
          'version' => '1.4.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../doctrine/instantiator',
          'aliases' => 
          array (
          ),
          'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
          'dev_requirement' => true,
        ),
        'doctrine/lexer' => 
        array (
          'pretty_version' => '1.2.1',
          'version' => '1.2.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../doctrine/lexer',
          'aliases' => 
          array (
          ),
          'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
          'dev_requirement' => false,
        ),
        'dragonmantank/cron-expression' => 
        array (
          'pretty_version' => 'v3.1.0',
          'version' => '3.1.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../dragonmantank/cron-expression',
          'aliases' => 
          array (
          ),
          'reference' => '7a8c6e56ab3ffcc538d05e8155bb42269abf1a0c',
          'dev_requirement' => false,
        ),
        'egulias/email-validator' => 
        array (
          'pretty_version' => '2.1.25',
          'version' => '2.1.25.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../egulias/email-validator',
          'aliases' => 
          array (
          ),
          'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
          'dev_requirement' => false,
        ),
        'fzaninotto/faker' => 
        array (
          'pretty_version' => 'dev-master',
          'version' => 'dev-master',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../fzaninotto/faker',
          'aliases' => 
          array (
            0 => '1.9.x-dev',
          ),
          'reference' => '5ffe7db6c80f441f150fc88008d64e64af66634b',
          'dev_requirement' => true,
        ),
        'graham-campbell/result-type' => 
        array (
          'pretty_version' => 'v1.0.2',
          'version' => '1.0.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../graham-campbell/result-type',
          'aliases' => 
          array (
          ),
          'reference' => '84afea85c6841deeea872f36249a206e878a5de0',
          'dev_requirement' => false,
        ),
        'hamcrest/hamcrest-php' => 
        array (
          'pretty_version' => 'v1.2.2',
          'version' => '1.2.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../hamcrest/hamcrest-php',
          'aliases' => 
          array (
          ),
          'reference' => 'b37020aa976fa52d3de9aa904aa2522dc518f79c',
          'dev_requirement' => true,
        ),
        'illuminate/auth' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/auth',
          'aliases' => 
          array (
          ),
          'reference' => '9158eb34589cd314234565d3c0a4ab93be8bd9d8',
          'dev_requirement' => false,
        ),
        'illuminate/broadcasting' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/broadcasting',
          'aliases' => 
          array (
          ),
          'reference' => '28afa2d498aa61ef52a7c1dc57da1fc1a24cabd3',
          'dev_requirement' => false,
        ),
        'illuminate/bus' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/bus',
          'aliases' => 
          array (
          ),
          'reference' => 'a222094903c473b6b0ade0b0b0e20b83ae1472b6',
          'dev_requirement' => false,
        ),
        'illuminate/cache' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/cache',
          'aliases' => 
          array (
          ),
          'reference' => 'f4dda85d48e8dc6000432db16176f2c9fa0ff08e',
          'dev_requirement' => false,
        ),
        'illuminate/collections' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/collections',
          'aliases' => 
          array (
          ),
          'reference' => '18fa841df912ec56849351dd6ca8928e8a98b69d',
          'dev_requirement' => false,
        ),
        'illuminate/config' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/config',
          'aliases' => 
          array (
          ),
          'reference' => '70973cbbe0cb524658b6eeaa2386dd5b71de4b02',
          'dev_requirement' => false,
        ),
        'illuminate/console' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/console',
          'aliases' => 
          array (
          ),
          'reference' => '417c16e14c4778fdb770d528f5e6396a5e6157ad',
          'dev_requirement' => false,
        ),
        'illuminate/container' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/container',
          'aliases' => 
          array (
          ),
          'reference' => 'ef73feb5216ef97ab7023cf59c0c8dbbd5505a9d',
          'dev_requirement' => false,
        ),
        'illuminate/contracts' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/contracts',
          'aliases' => 
          array (
          ),
          'reference' => 'ab4bb4ec3b36905ccf972c84f9aaa2bdd1153913',
          'dev_requirement' => false,
        ),
        'illuminate/database' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/database',
          'aliases' => 
          array (
          ),
          'reference' => '327c22dc8f117c07a3e00ecfb4f5011ede445407',
          'dev_requirement' => false,
        ),
        'illuminate/encryption' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/encryption',
          'aliases' => 
          array (
          ),
          'reference' => '3ff5c78f402c81da4b2ad4bef8f747a13e6fb0ff',
          'dev_requirement' => false,
        ),
        'illuminate/events' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/events',
          'aliases' => 
          array (
          ),
          'reference' => 'b7f06cafb6c09581617f2ca05d69e9b159e5a35d',
          'dev_requirement' => false,
        ),
        'illuminate/filesystem' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/filesystem',
          'aliases' => 
          array (
          ),
          'reference' => 'f33219e5550f8f280169e933b91a95250920de06',
          'dev_requirement' => false,
        ),
        'illuminate/hashing' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/hashing',
          'aliases' => 
          array (
          ),
          'reference' => 'e0541364324c4cc165d4fd54afade571e1bb1626',
          'dev_requirement' => false,
        ),
        'illuminate/http' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/http',
          'aliases' => 
          array (
          ),
          'reference' => 'e6f2673f960ea98aedd8b8cb0fc0635c45ee48dc',
          'dev_requirement' => false,
        ),
        'illuminate/log' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/log',
          'aliases' => 
          array (
          ),
          'reference' => '848190a210d4caaf06be5ea825e945a5fdba8297',
          'dev_requirement' => false,
        ),
        'illuminate/macroable' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/macroable',
          'aliases' => 
          array (
          ),
          'reference' => '300aa13c086f25116b5f3cde3ca54ff5c822fb05',
          'dev_requirement' => false,
        ),
        'illuminate/pagination' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/pagination',
          'aliases' => 
          array (
          ),
          'reference' => 'be5f9d27f36c5f771ec77ad2e6b841d29adc1cef',
          'dev_requirement' => false,
        ),
        'illuminate/pipeline' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/pipeline',
          'aliases' => 
          array (
          ),
          'reference' => '23aeff5b26ae4aee3f370835c76bd0f4e93f71d2',
          'dev_requirement' => false,
        ),
        'illuminate/queue' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/queue',
          'aliases' => 
          array (
          ),
          'reference' => 'f219c5f07df9c47ab50f4c3078027ec1e56f426b',
          'dev_requirement' => false,
        ),
        'illuminate/session' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/session',
          'aliases' => 
          array (
          ),
          'reference' => '9ed81f37664392e09d55058c96811d791311df21',
          'dev_requirement' => false,
        ),
        'illuminate/support' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/support',
          'aliases' => 
          array (
          ),
          'reference' => '3c5fe0dc1bf1ffae91356492e674de9a4fe33c4c',
          'dev_requirement' => false,
        ),
        'illuminate/testing' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/testing',
          'aliases' => 
          array (
          ),
          'reference' => 'fd06de78c512fd67904485957d48a657d4ffca42',
          'dev_requirement' => false,
        ),
        'illuminate/translation' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/translation',
          'aliases' => 
          array (
          ),
          'reference' => '28219a6b98b2f1de529c3a821c4efe494f4247d3',
          'dev_requirement' => false,
        ),
        'illuminate/validation' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/validation',
          'aliases' => 
          array (
          ),
          'reference' => '2de187cd06a6e1a3cccc0bebe76654e6ded19103',
          'dev_requirement' => false,
        ),
        'illuminate/view' => 
        array (
          'pretty_version' => 'v8.62.0',
          'version' => '8.62.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../illuminate/view',
          'aliases' => 
          array (
          ),
          'reference' => '875ca9f548b17e1a225469e0b0f8bae3c9e4ff71',
          'dev_requirement' => false,
        ),
        'kodova/hamcrest-php' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '*',
          ),
        ),
        'laravel/lumen-framework' => 
        array (
          'pretty_version' => 'v8.2.7',
          'version' => '8.2.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../laravel/lumen-framework',
          'aliases' => 
          array (
          ),
          'reference' => 'f5ae0d03da094577fef711120463eba116575487',
          'dev_requirement' => false,
        ),
        'laravel/serializable-closure' => 
        array (
          'pretty_version' => 'v1.0.2',
          'version' => '1.0.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../laravel/serializable-closure',
          'aliases' => 
          array (
          ),
          'reference' => '679e24d36ff8b9be0e36f5222244ec8602e18867',
          'dev_requirement' => false,
        ),
        'mockery/mockery' => 
        array (
          'pretty_version' => '0.9.11',
          'version' => '0.9.11.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../mockery/mockery',
          'aliases' => 
          array (
          ),
          'reference' => 'be9bf28d8e57d67883cba9fcadfcff8caab667f8',
          'dev_requirement' => true,
        ),
        'monolog/monolog' => 
        array (
          'pretty_version' => '2.3.4',
          'version' => '2.3.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../monolog/monolog',
          'aliases' => 
          array (
          ),
          'reference' => '437e7a1c50044b92773b361af77620efb76fff59',
          'dev_requirement' => false,
        ),
        'mtdowling/cron-expression' => 
        array (
          'dev_requirement' => false,
          'replaced' => 
          array (
            0 => '^1.0',
          ),
        ),
        'myclabs/deep-copy' => 
        array (
          'pretty_version' => '1.10.2',
          'version' => '1.10.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../myclabs/deep-copy',
          'aliases' => 
          array (
          ),
          'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '1.10.2',
          ),
        ),
        'nesbot/carbon' => 
        array (
          'pretty_version' => '2.53.1',
          'version' => '2.53.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../nesbot/carbon',
          'aliases' => 
          array (
          ),
          'reference' => 'f4655858a784988f880c1b8c7feabbf02dfdf045',
          'dev_requirement' => false,
        ),
        'nikic/fast-route' => 
        array (
          'pretty_version' => 'v1.3.0',
          'version' => '1.3.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../nikic/fast-route',
          'aliases' => 
          array (
          ),
          'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
          'dev_requirement' => false,
        ),
        'nikic/php-parser' => 
        array (
          'pretty_version' => 'v4.13.0',
          'version' => '4.13.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../nikic/php-parser',
          'aliases' => 
          array (
          ),
          'reference' => '50953a2691a922aa1769461637869a0a2faa3f53',
          'dev_requirement' => true,
        ),
        'opis/closure' => 
        array (
          'pretty_version' => '3.6.2',
          'version' => '3.6.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../opis/closure',
          'aliases' => 
          array (
          ),
          'reference' => '06e2ebd25f2869e54a306dda991f7db58066f7f6',
          'dev_requirement' => false,
        ),
        'phar-io/manifest' => 
        array (
          'pretty_version' => '2.0.3',
          'version' => '2.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phar-io/manifest',
          'aliases' => 
          array (
          ),
          'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
          'dev_requirement' => true,
        ),
        'phar-io/version' => 
        array (
          'pretty_version' => '3.1.0',
          'version' => '3.1.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phar-io/version',
          'aliases' => 
          array (
          ),
          'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
          'dev_requirement' => true,
        ),
        'phpdocumentor/reflection-common' => 
        array (
          'pretty_version' => '2.2.0',
          'version' => '2.2.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpdocumentor/reflection-common',
          'aliases' => 
          array (
          ),
          'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
          'dev_requirement' => true,
        ),
        'phpdocumentor/reflection-docblock' => 
        array (
          'pretty_version' => '5.2.2',
          'version' => '5.2.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpdocumentor/reflection-docblock',
          'aliases' => 
          array (
          ),
          'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
          'dev_requirement' => true,
        ),
        'phpdocumentor/type-resolver' => 
        array (
          'pretty_version' => '1.5.0',
          'version' => '1.5.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpdocumentor/type-resolver',
          'aliases' => 
          array (
          ),
          'reference' => '30f38bffc6f24293dadd1823936372dfa9e86e2f',
          'dev_requirement' => true,
        ),
        'phpoption/phpoption' => 
        array (
          'pretty_version' => '1.8.0',
          'version' => '1.8.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpoption/phpoption',
          'aliases' => 
          array (
          ),
          'reference' => '5455cb38aed4523f99977c4a12ef19da4bfe2a28',
          'dev_requirement' => false,
        ),
        'phpspec/prophecy' => 
        array (
          'pretty_version' => '1.14.0',
          'version' => '1.14.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpspec/prophecy',
          'aliases' => 
          array (
          ),
          'reference' => 'd86dfc2e2a3cd366cee475e52c6bb3bbc371aa0e',
          'dev_requirement' => true,
        ),
        'phpstan/phpstan' => 
        array (
          'pretty_version' => '0.12.99',
          'version' => '0.12.99.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpstan/phpstan',
          'aliases' => 
          array (
          ),
          'reference' => 'b4d40f1d759942f523be267a1bab6884f46ca3f7',
          'dev_requirement' => true,
        ),
        'phpunit/php-code-coverage' => 
        array (
          'pretty_version' => '9.2.7',
          'version' => '9.2.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpunit/php-code-coverage',
          'aliases' => 
          array (
          ),
          'reference' => 'd4c798ed8d51506800b441f7a13ecb0f76f12218',
          'dev_requirement' => true,
        ),
        'phpunit/php-file-iterator' => 
        array (
          'pretty_version' => '3.0.5',
          'version' => '3.0.5.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpunit/php-file-iterator',
          'aliases' => 
          array (
          ),
          'reference' => 'aa4be8575f26070b100fccb67faabb28f21f66f8',
          'dev_requirement' => true,
        ),
        'phpunit/php-invoker' => 
        array (
          'pretty_version' => '3.1.1',
          'version' => '3.1.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpunit/php-invoker',
          'aliases' => 
          array (
          ),
          'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
          'dev_requirement' => true,
        ),
        'phpunit/php-text-template' => 
        array (
          'pretty_version' => '2.0.4',
          'version' => '2.0.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpunit/php-text-template',
          'aliases' => 
          array (
          ),
          'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
          'dev_requirement' => true,
        ),
        'phpunit/php-timer' => 
        array (
          'pretty_version' => '5.0.3',
          'version' => '5.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpunit/php-timer',
          'aliases' => 
          array (
          ),
          'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
          'dev_requirement' => true,
        ),
        'phpunit/phpunit' => 
        array (
          'pretty_version' => '9.5.10',
          'version' => '9.5.10.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../phpunit/phpunit',
          'aliases' => 
          array (
          ),
          'reference' => 'c814a05837f2edb0d1471d6e3f4ab3501ca3899a',
          'dev_requirement' => true,
        ),
        'psr/container' => 
        array (
          'pretty_version' => '1.1.1',
          'version' => '1.1.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../psr/container',
          'aliases' => 
          array (
          ),
          'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
          'dev_requirement' => false,
        ),
        'psr/container-implementation' => 
        array (
          'dev_requirement' => false,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'psr/event-dispatcher' => 
        array (
          'pretty_version' => '1.0.0',
          'version' => '1.0.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../psr/event-dispatcher',
          'aliases' => 
          array (
          ),
          'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
          'dev_requirement' => false,
        ),
        'psr/event-dispatcher-implementation' => 
        array (
          'dev_requirement' => false,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'psr/log' => 
        array (
          'pretty_version' => '1.1.4',
          'version' => '1.1.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../psr/log',
          'aliases' => 
          array (
          ),
          'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
          'dev_requirement' => false,
        ),
        'psr/log-implementation' => 
        array (
          'dev_requirement' => false,
          'provided' => 
          array (
            0 => '1.0.0 || 2.0.0 || 3.0.0',
            1 => '1.0|2.0',
          ),
        ),
        'psr/simple-cache' => 
        array (
          'pretty_version' => '1.0.1',
          'version' => '1.0.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../psr/simple-cache',
          'aliases' => 
          array (
          ),
          'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
          'dev_requirement' => false,
        ),
        'psr/simple-cache-implementation' => 
        array (
          'dev_requirement' => false,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'ramsey/collection' => 
        array (
          'pretty_version' => '1.2.1',
          'version' => '1.2.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../ramsey/collection',
          'aliases' => 
          array (
          ),
          'reference' => 'eaca1dc1054ddd10cbd83c1461907bee6fb528fa',
          'dev_requirement' => false,
        ),
        'ramsey/uuid' => 
        array (
          'pretty_version' => '4.2.3',
          'version' => '4.2.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../ramsey/uuid',
          'aliases' => 
          array (
          ),
          'reference' => 'fc9bb7fb5388691fd7373cd44dcb4d63bbcf24df',
          'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => 
        array (
          'dev_requirement' => false,
          'replaced' => 
          array (
            0 => '4.2.3',
          ),
        ),
        'sebastian/cli-parser' => 
        array (
          'pretty_version' => '1.0.1',
          'version' => '1.0.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/cli-parser',
          'aliases' => 
          array (
          ),
          'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
          'dev_requirement' => true,
        ),
        'sebastian/code-unit' => 
        array (
          'pretty_version' => '1.0.8',
          'version' => '1.0.8.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/code-unit',
          'aliases' => 
          array (
          ),
          'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
          'dev_requirement' => true,
        ),
        'sebastian/code-unit-reverse-lookup' => 
        array (
          'pretty_version' => '2.0.3',
          'version' => '2.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/code-unit-reverse-lookup',
          'aliases' => 
          array (
          ),
          'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
          'dev_requirement' => true,
        ),
        'sebastian/comparator' => 
        array (
          'pretty_version' => '4.0.6',
          'version' => '4.0.6.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/comparator',
          'aliases' => 
          array (
          ),
          'reference' => '55f4261989e546dc112258c7a75935a81a7ce382',
          'dev_requirement' => true,
        ),
        'sebastian/complexity' => 
        array (
          'pretty_version' => '2.0.2',
          'version' => '2.0.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/complexity',
          'aliases' => 
          array (
          ),
          'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
          'dev_requirement' => true,
        ),
        'sebastian/diff' => 
        array (
          'pretty_version' => '4.0.4',
          'version' => '4.0.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/diff',
          'aliases' => 
          array (
          ),
          'reference' => '3461e3fccc7cfdfc2720be910d3bd73c69be590d',
          'dev_requirement' => true,
        ),
        'sebastian/environment' => 
        array (
          'pretty_version' => '5.1.3',
          'version' => '5.1.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/environment',
          'aliases' => 
          array (
          ),
          'reference' => '388b6ced16caa751030f6a69e588299fa09200ac',
          'dev_requirement' => true,
        ),
        'sebastian/exporter' => 
        array (
          'pretty_version' => '4.0.3',
          'version' => '4.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/exporter',
          'aliases' => 
          array (
          ),
          'reference' => 'd89cc98761b8cb5a1a235a6b703ae50d34080e65',
          'dev_requirement' => true,
        ),
        'sebastian/global-state' => 
        array (
          'pretty_version' => '5.0.3',
          'version' => '5.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/global-state',
          'aliases' => 
          array (
          ),
          'reference' => '23bd5951f7ff26f12d4e3242864df3e08dec4e49',
          'dev_requirement' => true,
        ),
        'sebastian/lines-of-code' => 
        array (
          'pretty_version' => '1.0.3',
          'version' => '1.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/lines-of-code',
          'aliases' => 
          array (
          ),
          'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
          'dev_requirement' => true,
        ),
        'sebastian/object-enumerator' => 
        array (
          'pretty_version' => '4.0.4',
          'version' => '4.0.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/object-enumerator',
          'aliases' => 
          array (
          ),
          'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
          'dev_requirement' => true,
        ),
        'sebastian/object-reflector' => 
        array (
          'pretty_version' => '2.0.4',
          'version' => '2.0.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/object-reflector',
          'aliases' => 
          array (
          ),
          'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
          'dev_requirement' => true,
        ),
        'sebastian/recursion-context' => 
        array (
          'pretty_version' => '4.0.4',
          'version' => '4.0.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/recursion-context',
          'aliases' => 
          array (
          ),
          'reference' => 'cd9d8cf3c5804de4341c283ed787f099f5506172',
          'dev_requirement' => true,
        ),
        'sebastian/resource-operations' => 
        array (
          'pretty_version' => '3.0.3',
          'version' => '3.0.3.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/resource-operations',
          'aliases' => 
          array (
          ),
          'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
          'dev_requirement' => true,
        ),
        'sebastian/type' => 
        array (
          'pretty_version' => '2.3.4',
          'version' => '2.3.4.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/type',
          'aliases' => 
          array (
          ),
          'reference' => 'b8cd8a1c753c90bc1a0f5372170e3e489136f914',
          'dev_requirement' => true,
        ),
        'sebastian/version' => 
        array (
          'pretty_version' => '3.0.2',
          'version' => '3.0.2.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../sebastian/version',
          'aliases' => 
          array (
          ),
          'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
          'dev_requirement' => true,
        ),
        'symfony/console' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/console',
          'aliases' => 
          array (
          ),
          'reference' => '8b1008344647462ae6ec57559da166c2bfa5e16a',
          'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => 
        array (
          'pretty_version' => 'v2.4.0',
          'version' => '2.4.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/deprecation-contracts',
          'aliases' => 
          array (
          ),
          'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
          'dev_requirement' => false,
        ),
        'symfony/error-handler' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/error-handler',
          'aliases' => 
          array (
          ),
          'reference' => '3bc60d0fba00ae8d1eaa9eb5ab11a2bbdd1fc321',
          'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/event-dispatcher',
          'aliases' => 
          array (
          ),
          'reference' => 'ce7b20d69c66a20939d8952b617506a44d102130',
          'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-contracts' => 
        array (
          'pretty_version' => 'v2.4.0',
          'version' => '2.4.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/event-dispatcher-contracts',
          'aliases' => 
          array (
          ),
          'reference' => '69fee1ad2332a7cbab3aca13591953da9cdb7a11',
          'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-implementation' => 
        array (
          'dev_requirement' => false,
          'provided' => 
          array (
            0 => '2.0',
          ),
        ),
        'symfony/finder' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/finder',
          'aliases' => 
          array (
          ),
          'reference' => 'a10000ada1e600d109a6c7632e9ac42e8bf2fb93',
          'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => 
        array (
          'pretty_version' => 'v2.4.0',
          'version' => '2.4.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/http-client-contracts',
          'aliases' => 
          array (
          ),
          'reference' => '7e82f6084d7cae521a75ef2cb5c9457bbda785f4',
          'dev_requirement' => false,
        ),
        'symfony/http-foundation' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/http-foundation',
          'aliases' => 
          array (
          ),
          'reference' => 'e36c8e5502b4f3f0190c675f1c1f1248a64f04e5',
          'dev_requirement' => false,
        ),
        'symfony/http-kernel' => 
        array (
          'pretty_version' => 'v5.3.9',
          'version' => '5.3.9.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/http-kernel',
          'aliases' => 
          array (
          ),
          'reference' => 'ceaf46a992f60e90645e7279825a830f733a17c5',
          'dev_requirement' => false,
        ),
        'symfony/mime' => 
        array (
          'pretty_version' => 'v5.3.8',
          'version' => '5.3.8.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/mime',
          'aliases' => 
          array (
          ),
          'reference' => 'a756033d0a7e53db389618653ae991eba5a19a11',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => 
        array (
          'pretty_version' => 'v1.23.0',
          'version' => '1.23.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-ctype',
          'aliases' => 
          array (
          ),
          'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => 
        array (
          'pretty_version' => 'v1.23.1',
          'version' => '1.23.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-intl-grapheme',
          'aliases' => 
          array (
          ),
          'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => 
        array (
          'pretty_version' => 'v1.23.0',
          'version' => '1.23.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-intl-idn',
          'aliases' => 
          array (
          ),
          'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => 
        array (
          'pretty_version' => 'v1.23.0',
          'version' => '1.23.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-intl-normalizer',
          'aliases' => 
          array (
          ),
          'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => 
        array (
          'pretty_version' => 'v1.23.1',
          'version' => '1.23.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-mbstring',
          'aliases' => 
          array (
          ),
          'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => 
        array (
          'pretty_version' => 'v1.23.0',
          'version' => '1.23.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-php72',
          'aliases' => 
          array (
          ),
          'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => 
        array (
          'pretty_version' => 'v1.23.0',
          'version' => '1.23.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-php73',
          'aliases' => 
          array (
          ),
          'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => 
        array (
          'pretty_version' => 'v1.23.1',
          'version' => '1.23.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-php80',
          'aliases' => 
          array (
          ),
          'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
          'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => 
        array (
          'pretty_version' => 'v1.23.0',
          'version' => '1.23.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/polyfill-php81',
          'aliases' => 
          array (
          ),
          'reference' => 'e66119f3de95efc359483f810c4c3e6436279436',
          'dev_requirement' => false,
        ),
        'symfony/process' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/process',
          'aliases' => 
          array (
          ),
          'reference' => '38f26c7d6ed535217ea393e05634cb0b244a1967',
          'dev_requirement' => false,
        ),
        'symfony/service-contracts' => 
        array (
          'pretty_version' => 'v2.4.0',
          'version' => '2.4.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/service-contracts',
          'aliases' => 
          array (
          ),
          'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
          'dev_requirement' => false,
        ),
        'symfony/string' => 
        array (
          'pretty_version' => 'v5.3.7',
          'version' => '5.3.7.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/string',
          'aliases' => 
          array (
          ),
          'reference' => '8d224396e28d30f81969f083a58763b8b9ceb0a5',
          'dev_requirement' => false,
        ),
        'symfony/translation' => 
        array (
          'pretty_version' => 'v5.3.9',
          'version' => '5.3.9.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/translation',
          'aliases' => 
          array (
          ),
          'reference' => '6e69f3551c1a3356cf6ea8d019bf039a0f8b6886',
          'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => 
        array (
          'pretty_version' => 'v2.4.0',
          'version' => '2.4.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/translation-contracts',
          'aliases' => 
          array (
          ),
          'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
          'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => 
        array (
          'dev_requirement' => false,
          'provided' => 
          array (
            0 => '2.3',
          ),
        ),
        'symfony/var-dumper' => 
        array (
          'pretty_version' => 'v5.3.8',
          'version' => '5.3.8.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../symfony/var-dumper',
          'aliases' => 
          array (
          ),
          'reference' => 'eaaea4098be1c90c8285543e1356a09c8aa5c8da',
          'dev_requirement' => false,
        ),
        'theseer/tokenizer' => 
        array (
          'pretty_version' => '1.2.1',
          'version' => '1.2.1.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../theseer/tokenizer',
          'aliases' => 
          array (
          ),
          'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
          'dev_requirement' => true,
        ),
        'vlucas/phpdotenv' => 
        array (
          'pretty_version' => 'v5.3.0',
          'version' => '5.3.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../vlucas/phpdotenv',
          'aliases' => 
          array (
          ),
          'reference' => 'b3eac5c7ac896e52deab4a99068e3f4ab12d9e56',
          'dev_requirement' => false,
        ),
        'voku/portable-ascii' => 
        array (
          'pretty_version' => '1.5.6',
          'version' => '1.5.6.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../voku/portable-ascii',
          'aliases' => 
          array (
          ),
          'reference' => '80953678b19901e5165c56752d087fc11526017c',
          'dev_requirement' => false,
        ),
        'webmozart/assert' => 
        array (
          'pretty_version' => '1.10.0',
          'version' => '1.10.0.0',
          'type' => 'library',
          'install_path' => '/var/www/html/vendor/composer/../webmozart/assert',
          'aliases' => 
          array (
          ),
          'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
          'dev_requirement' => false,
        ),
      ),
    ),
  ),
  'executedFilesHashes' => 
  array (
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/Attribute.php' => '4afed65b0d2cbfac6b7819f42dea6d23f4c8e47b',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/ReflectionAttribute.php' => '7f057354c4b06042bed673ec4f98211629fe3584',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/ReflectionUnionType.php' => '1b349aa997a834faeafe05fa21bc31cae22bf2e2',
  ),
  'phpExtensions' => 
  array (
    0 => 'Core',
    1 => 'PDO',
    2 => 'Phar',
    3 => 'Reflection',
    4 => 'SPL',
    5 => 'SimpleXML',
    6 => 'ctype',
    7 => 'curl',
    8 => 'date',
    9 => 'dom',
    10 => 'fileinfo',
    11 => 'filter',
    12 => 'ftp',
    13 => 'gd',
    14 => 'hash',
    15 => 'iconv',
    16 => 'json',
    17 => 'libxml',
    18 => 'mbstring',
    19 => 'mysqli',
    20 => 'mysqlnd',
    21 => 'openssl',
    22 => 'pcre',
    23 => 'pdo_mysql',
    24 => 'pdo_sqlite',
    25 => 'posix',
    26 => 'readline',
    27 => 'session',
    28 => 'sodium',
    29 => 'sqlite3',
    30 => 'standard',
    31 => 'tokenizer',
    32 => 'xml',
    33 => 'xmlreader',
    34 => 'xmlwriter',
    35 => 'zip',
    36 => 'zlib',
  ),
  'stubFiles' => 
  array (
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ArrayObject.stub' => '34c9e85af3fe514ea3e18f7fc87e03f7890e3a46',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/Exception.stub' => '82914ad49fd789e22b8648daf76335a98616d7a8',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/PDOStatement.stub' => '7ed5b8ee9a46556e28551a26f7501a85ac343483',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ReflectionAttribute.stub' => '614c3dcc562c65118608b1c4e1176684ce1a2cc5',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ReflectionClass.stub' => '78d777787f0085b0b72f207c241355d82d27e77a',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ReflectionClassConstant.stub' => '7c0f2ee95301fed439a48f7d55e5d4d1487c9bb8',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ReflectionFunctionAbstract.stub' => '2196325c2433ae733e0e7993400429493a1cd342',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ReflectionParameter.stub' => '3e648b120364fd63447250d11819d6206c9b03e9',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ReflectionProperty.stub' => '78f68f13d18db0d71458216a47e9c8cec4b76024',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/SplObjectStorage.stub' => '8c1272fb5f32ab3478e0c805276609cd02277ee4',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/WeakReference.stub' => '2fadf02e9175d02a0d39240d89ae38bc939aa605',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/date.stub' => '8d63f9636060e7efdfced52e29873f51c7cab46e',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/dom.stub' => '850c98e54136d3dbbd46c1042a9286f7ca4d36f0',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/ext-ds.stub' => 'd6ee3aa03606b7a32ba2da3fcaa9bd725b28a868',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/iterable.stub' => 'c9f8f94d850ef9ed7e79aecba40f182cccbc45b7',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/spl.stub' => '6a8ce07b7dc82139ffd4ab1e84414e984801b065',
    'phar:///var/www/html/vendor/phpstan/phpstan/phpstan.phar/stubs/zip.stub' => 'aff61beeb3e9dfaed0eede432921df5eee2d3b40',
  ),
  'level' => 'max',
),
	'projectExtensionFiles' => array (
),
	'errorsCallback' => static function (): array { return array (
); },
	'dependencies' => array (
  '/var/www/html/tests/ListingTest.php' => 
  array (
    'fileHash' => 'db7bd02c76c916a880c863fe2ef35ef014db843e',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/tests/TestCase.php' => 
  array (
    'fileHash' => 'ec1c4bd90cb3cbbb9ba4958d7974dfba8f53e7ca',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/tests/ListingTest.php',
    ),
  ),
),
	'exportedNodesCallback' => static function (): array { return array (
  '/var/www/html/tests/ListingTest.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'ListingTest',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'TestCase',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
    )),
    1 => 
    PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
       'name' => 'testListingAPIEndpoint',
       'phpDoc' => 
      PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
         'phpDocString' => '/**
     * Test the listing api endpoint.
     *
     * @return void
     */',
         'namespace' => NULL,
         'uses' => 
        array (
          'databasemigrations' => 'Laravel\\Lumen\\Testing\\DatabaseMigrations',
          'databasetransactions' => 'Laravel\\Lumen\\Testing\\DatabaseTransactions',
        ),
      )),
       'byRef' => false,
       'public' => true,
       'private' => false,
       'abstract' => false,
       'final' => false,
       'static' => false,
       'returnType' => NULL,
       'parameters' => 
      array (
      ),
    )),
  ),
  '/var/www/html/tests/TestCase.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'TestCase',
       'phpDoc' => NULL,
       'abstract' => true,
       'final' => false,
       'extends' => 'Laravel\\Lumen\\Testing\\TestCase',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
    )),
    1 => 
    PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
       'name' => 'createApplication',
       'phpDoc' => 
      PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
         'phpDocString' => '/**
     * Creates the application.
     *
     * @return \\Laravel\\Lumen\\Application
     */',
         'namespace' => NULL,
         'uses' => 
        array (
        ),
      )),
       'byRef' => false,
       'public' => true,
       'private' => false,
       'abstract' => false,
       'final' => false,
       'static' => false,
       'returnType' => NULL,
       'parameters' => 
      array (
      ),
    )),
  ),
); },
];