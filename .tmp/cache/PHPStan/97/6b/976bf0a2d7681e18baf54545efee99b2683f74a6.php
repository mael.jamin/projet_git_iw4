<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/app/Console/Kernel.php-1632944936',
   'data' => 
  array (
    '3ac01e822614d5457ba8152f2e810a31' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The Artisan commands provided by your application.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'App\\Console',
         'uses' => 
        array (
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'consolekernel' => 'Laravel\\Lumen\\Console\\Kernel',
        ),
         'className' => 'App\\Console\\Kernel',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4e5db55db1d7064d8b6db73e2b3ca5cc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define the application\'s command schedule.
     *
     * @param  \\Illuminate\\Console\\Scheduling\\Schedule  $schedule
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'App\\Console',
         'uses' => 
        array (
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'consolekernel' => 'Laravel\\Lumen\\Console\\Kernel',
        ),
         'className' => 'App\\Console\\Kernel',
         'functionName' => 'schedule',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));