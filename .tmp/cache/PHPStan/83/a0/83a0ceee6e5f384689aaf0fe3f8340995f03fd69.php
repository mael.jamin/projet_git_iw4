<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/vendor/illuminate/testing/TestResponse.php-1632231479,/var/www/html/vendor/illuminate/support/Traits/Tappable.php-1632754679,/var/www/html/vendor/illuminate/macroable/Traits/Macroable.php-1603812030',
   'data' => 
  array (
    'b38323de925750123764bf7fd936c994' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
 * @mixin \\Illuminate\\Http\\Response
 */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1106f2e86cf244213bfb915897e31f72' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Call the given Closure with this instance then return the instance.
     *
     * @param  callable|null  $callback
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'tap',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '864b2cf7ffa20a99195fbe20538a0eb4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The registered string macros.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd61bd662c9e32ee325d2d7d621856e0b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a custom macro.
     *
     * @param  string  $name
     * @param  object|callable  $macro
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'macro',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '79398f70b292eb591a1734a225118232' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Mix another object into the class.
     *
     * @param  object  $mixin
     * @param  bool  $replace
     * @return void
     *
     * @throws \\ReflectionException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'mixin',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '761445731643ea97fe176bcc5dcc7489' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Checks if macro is registered.
     *
     * @param  string  $name
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'hasMacro',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '42a688addf129a6b4821c0ebcf82a1d8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically handle calls to the class.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => '__callStatic',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0a9a410ade2db87328c21c56e8db3744' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically handle calls to the class.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => '__call',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '43575417c257e9d865c87b6be4cd4ef7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The response to delegate to.
     *
     * @var \\Illuminate\\Http\\Response
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '43657f7ec63b52f3f7f3d40e500233ea' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The collection of logged exceptions for the request.
     *
     * @var \\Illuminate\\Support\\Collection
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7d7856f7f3adf669f9ae6051b8ee42d7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The streamed content of the response.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6467e610ec64382f44664fd505347d8d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new test response instance.
     *
     * @param  \\Illuminate\\Http\\Response  $response
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => '__construct',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '261f921152878a55cb0d417ac668b561' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new TestResponse from another response.
     *
     * @param  \\Illuminate\\Http\\Response  $response
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'fromBaseResponse',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '53f53493336d17e3a548448c435bad9a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a successful status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSuccessful',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e1707451da711b3763ebedee700e0e62' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a 200 status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertOk',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '84f8ecf383375e8b6665427589b23351' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a 201 status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertCreated',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd1de244a4068d6ddb58ba2305966637c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has the given status code and no content.
     *
     * @param  int  $status
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertNoContent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd709fff3a43238e9bc2c8b870880f5c8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a not found status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertNotFound',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '45048fd031fb6976749c824f45222477' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a forbidden status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertForbidden',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f44ac942b75ce34abe21d162a54b795e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has an unauthorized status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertUnauthorized',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9b544a1d80a1c373d290240eff10a9f2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a 422 status code.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertUnprocessable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'deb6f15c10b9966718b7445b98c74a5d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has the given status code.
     *
     * @param  int  $status
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertStatus',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2c55e1433880bc42d9b014266c4f2419' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an assertion message for a status assertion containing extra details when available.
     *
     * @param  string|int  $expected
     * @param  string|int  $actual
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'statusMessageWithDetails',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b04d8a23fc9397b62a40e79d654815c8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an assertion message for a status assertion that has an unexpected exception.
     *
     * @param  string|int  $expected
     * @param  string|int  $actual
     * @param  \\Throwable  $exception
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'statusMessageWithException',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '52b55db023102d010709a7b1c944f489' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an assertion message for a status assertion that contained errors.
     *
     * @param  string|int  $expected
     * @param  string|int  $actual
     * @param  array  $errors
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'statusMessageWithErrors',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0c9469f8450c562aacb6d70dfcd738b7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert whether the response is redirecting to a given URI.
     *
     * @param  string|null  $uri
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertRedirect',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '64751dd3e8d3729eabdf6fe158e9ec71' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert whether the response is redirecting to a given signed route.
     *
     * @param  string|null  $name
     * @param  mixed  $parameters
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertRedirectToSignedRoute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b00b3d12384109a45057b4db958c5ce8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response contains the given header and equals the optional value.
     *
     * @param  string  $headerName
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertHeader',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8c94c86d0c41979611f7a386e4eefe6f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response does not contain the given header.
     *
     * @param  string  $headerName
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertHeaderMissing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a4d3992dec5bbae8da19a0286061484c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the current location header matches the given URI.
     *
     * @param  string  $uri
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertLocation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ecdb69ff73fbbf65038d92452c12c998' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response offers a file download.
     *
     * @param  string|null  $filename
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertDownload',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b7c1f4f76f09412ef5d2e26ab70e9a11' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response contains the given cookie and equals the optional value.
     *
     * @param  string  $cookieName
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertPlainCookie',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7b830c286cccd4d966441b0b26da798e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response contains the given cookie and equals the optional value.
     *
     * @param  string  $cookieName
     * @param  mixed  $value
     * @param  bool  $encrypted
     * @param  bool  $unserialize
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertCookie',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c4957c6b7166f175d4e68467b68c6ea7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response contains the given cookie and is expired.
     *
     * @param  string  $cookieName
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertCookieExpired',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '35937cbf7c23834b50b94e8fe443234f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response contains the given cookie and is not expired.
     *
     * @param  string  $cookieName
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertCookieNotExpired',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ec2161595a5e44849f96f14c1d39bee2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response does not contain the given cookie.
     *
     * @param  string  $cookieName
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertCookieMissing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9284115788a6de54d27390fa9284e9f6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the given cookie from the response.
     *
     * @param  string  $cookieName
     * @return \\Symfony\\Component\\HttpFoundation\\Cookie|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'getCookie',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1f34f00be46383ada7c4f163826b1766' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given string or array of strings are contained within the response.
     *
     * @param  string|array  $value
     * @param  bool  $escape
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSee',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1dcd134a6eee62c24b9df2740d8fc511' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given strings are contained in order within the response.
     *
     * @param  array  $values
     * @param  bool  $escape
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSeeInOrder',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba18f5baa219e798324d1241fc9ec9ac' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given string or array of strings are contained within the response text.
     *
     * @param  string|array  $value
     * @param  bool  $escape
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSeeText',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae20345a11a224e02222ceb429f9730e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given strings are contained in order within the response text.
     *
     * @param  array  $values
     * @param  bool  $escape
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSeeTextInOrder',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bba2d243562effa6b0935023818d6cb8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given string or array of strings are not contained within the response.
     *
     * @param  string|array  $value
     * @param  bool  $escape
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertDontSee',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7af644887f2fd5d209072dc076277c33' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given string or array of strings are not contained within the response text.
     *
     * @param  string|array  $value
     * @param  bool  $escape
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertDontSeeText',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a05e43d079bc6405802e028cf57dde7f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response is a superset of the given JSON.
     *
     * @param  array|callable  $value
     * @param  bool  $strict
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd9cacab1fbd21441530be4ec0cbe2f8c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the expected value and type exists at the given path in the response.
     *
     * @param  string  $path
     * @param  mixed  $expect
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonPath',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '14268386a77937fa6051d73a4d0bc593' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has the exact given JSON.
     *
     * @param  array  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertExactJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f1dde7a7211facb910b1dadc80f80690' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has the similar JSON as given.
     *
     * @param  array  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSimilarJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f0f81dc299cddbecf782287da0ff5318' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response contains the given JSON fragment.
     *
     * @param  array  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonFragment',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c77fbde6ec58d8b06b4a239ce798dd46' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response does not contain the given JSON fragment.
     *
     * @param  array  $data
     * @param  bool  $exact
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonMissing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6be9745223a7574bbf993c8e2730bb3a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response does not contain the exact JSON fragment.
     *
     * @param  array  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonMissingExact',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '49e9f54687a502a3c0b93310d5b74ded' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has a given JSON structure.
     *
     * @param  array|null  $structure
     * @param  array|null  $responseData
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonStructure',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9dfaeebae18edc958c6cfb2a5e025cca' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response JSON has the expected count of items at the given key.
     *
     * @param  int  $count
     * @param  string|null  $key
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4c2a535ed11dc5f07a440fc0ac6480fe' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has the given JSON validation errors.
     *
     * @param  string|array  $errors
     * @param  string  $responseKey
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonValidationErrors',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '72e78cd1c78ec71c2ac082f12b9a687a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has no JSON validation errors for the given keys.
     *
     * @param  string|array|null  $keys
     * @param  string  $responseKey
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertJsonMissingValidationErrors',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a0dff95f76f6c10d172c41b32049b48f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Validate and return the decoded response JSON.
     *
     * @return \\Illuminate\\Testing\\AssertableJsonString
     *
     * @throws \\Throwable
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'decodeResponseJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'af8078343159703a60191cd59f270218' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Validate and return the decoded response JSON.
     *
     * @param  string|null  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'json',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cde13243f8412c97275d36f0648441ae' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response view equals the given value.
     *
     * @param  string  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertViewIs',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a6333e4c790f93b821c17b5c913b14a3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response view has a given piece of bound data.
     *
     * @param  string|array  $key
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertViewHas',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '57150cebd09a069b10c82a0e5812162a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response view has a given list of bound data.
     *
     * @param  array  $bindings
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertViewHasAll',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6c797cd099284e1a3f6d070ccf7e33c2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a piece of data from the original view.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'viewData',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '08d67c73926978dd6baf14c85f75d5aa' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response view is missing a piece of bound data.
     *
     * @param  string  $key
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertViewMissing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '443eedd0e9a7e4f3c8032de1ab747d0d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Ensure that the response has a view as its original content.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'ensureResponseHasView',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e934227787d7139cb36ea37bdb71dcd4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the original response is a view.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'responseHasView',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2ec5155bce23da0e00fc6165137328be' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the given keys do not have validation errors.
     *
     * @param  string|array|null  $keys
     * @param  string  $errorBag
     * @param  string  $responseKey
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertValid',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f38af74cd60d568d7fa370e69c9e9323' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response has the given validation errors.
     *
     * @param  string|array|null  $errors
     * @param  string  $errorBag
     * @param  string  $responseKey
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertInvalid',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6124db85b93f958b1795b8a5361a557b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session has a given value.
     *
     * @param  string|array  $key
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionHas',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '945fd02c7acb9767aab3ce59195f1df2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session has a given list of values.
     *
     * @param  array  $bindings
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionHasAll',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dc6083b0bcfb19abf5eed6ef6a86777d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session has a given value in the flashed input array.
     *
     * @param  string|array  $key
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionHasInput',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '99e019f9b8f6f1c3287cf7d393fc5e1f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session has the given errors.
     *
     * @param  string|array  $keys
     * @param  mixed  $format
     * @param  string  $errorBag
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionHasErrors',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3a0488635e78ecf635ce74c331c13e27' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session is missing the given errors.
     *
     * @param  string|array  $keys
     * @param  string|null  $format
     * @param  string  $errorBag
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionDoesntHaveErrors',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '457566907b09a7a5fb535c3564e5c55a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session has no errors.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionHasNoErrors',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '587df4f83169972d620b02630f6aa9d4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session has the given errors.
     *
     * @param  string  $errorBag
     * @param  string|array  $keys
     * @param  mixed  $format
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionHasErrorsIn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4075602edb36c0389a89b7fd86f4c0b1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the session does not have a given key.
     *
     * @param  string|array  $key
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'assertSessionMissing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9f4d65daa638013b6aa972a2daa172c5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the current session store.
     *
     * @return \\Illuminate\\Session\\Store
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'session',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5d01677c16264a44310e7647a9281db9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dump the content from the response.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'dump',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5026fd8c045b8dde6cc718489cbcd5f2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dump the headers from the response.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'dumpHeaders',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '469cffd380fbaae7ca125006c09717f0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dump the session from the response.
     *
     * @param  string|array  $keys
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'dumpSession',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9b48839c92bedb564bc4b552853f2dd2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the streamed content from the response.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'streamedContent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '043010ebfd53f27bddaa3603fdb9459f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the previous exceptions on the response.
     *
     * @param  \\Illuminate\\Support\\Collection  $exceptions
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'withExceptions',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '563c1e270dba983c6e37b9804db166df' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically access base response parameters.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => '__get',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'baaa7a28130499f0b6cf9406ecab8771' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Proxy isset() checks to the underlying base response.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => '__isset',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ab810191728eb3cd0127e31c910f02e0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given offset exists.
     *
     * @param  string  $offset
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'offsetExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cc1c25ad22120d37dfcc72bedc49310f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value for a given offset.
     *
     * @param  string  $offset
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'offsetGet',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c7f789757fc1c8da069f5abc282969eb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the value at the given offset.
     *
     * @param  string  $offset
     * @param  mixed  $value
     * @return void
     *
     * @throws \\LogicException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'offsetSet',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1deedbfef8314e5ff80ad710318487ea' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset the value at the given offset.
     *
     * @param  string  $offset
     * @return void
     *
     * @throws \\LogicException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => 'offsetUnset',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '25bdd3cb8777bb69968d2e3077c37bfb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Handle dynamic calls into macros or pass missing methods to the base response.
     *
     * @param  string  $method
     * @param  array  $args
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Testing',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'closure' => 'Closure',
          'view' => 'Illuminate\\Contracts\\View\\View',
          'cookievalueprefix' => 'Illuminate\\Cookie\\CookieValuePrefix',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'redirectresponse' => 'Illuminate\\Http\\RedirectResponse',
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'collection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'tappable' => 'Illuminate\\Support\\Traits\\Tappable',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'seeinorder' => 'Illuminate\\Testing\\Constraints\\SeeInOrder',
          'assertablejson' => 'Illuminate\\Testing\\Fluent\\AssertableJson',
          'logicexception' => 'LogicException',
          'streamedresponse' => 'Symfony\\Component\\HttpFoundation\\StreamedResponse',
        ),
         'className' => 'Illuminate\\Testing\\TestResponse',
         'functionName' => '__call',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));