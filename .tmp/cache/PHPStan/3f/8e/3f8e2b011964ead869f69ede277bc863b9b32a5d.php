<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/tests/ListingTest.php-1632944936',
   'data' => 
  array (
    'b6bc32ae2f486a9107033364dc94fa82' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Test the listing api endpoint.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => NULL,
         'uses' => 
        array (
          'databasemigrations' => 'Laravel\\Lumen\\Testing\\DatabaseMigrations',
          'databasetransactions' => 'Laravel\\Lumen\\Testing\\DatabaseTransactions',
        ),
         'className' => 'ListingTest',
         'functionName' => 'testListingAPIEndpoint',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));