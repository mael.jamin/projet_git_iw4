<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/vendor/illuminate/http/JsonResponse.php-1632329306,/var/www/html/vendor/illuminate/http/ResponseTrait.php-1632329306,/var/www/html/vendor/illuminate/macroable/Traits/Macroable.php-1603812030',
   'data' => 
  array (
    'd94af25e62ba50ed863d6bba60b4b11d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The original content of the response.
     *
     * @var mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd810ea3ee9223139fad31b3848d864c7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The exception that triggered the error response (if applicable).
     *
     * @var \\Throwable|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2c8d25e980943026a3666c61415ebabc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the status code for the response.
     *
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'status',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0b23f22515b9f1256eca34055a5d6da8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the status text for the response.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'statusText',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '28831c3a9e0b4772f6b6e8a40f7de5e2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the content of the response.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'content',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'da2f10c5d4ff60fcc0603833640f4c01' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the original response content.
     *
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'getOriginalContent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '75109685e0e20152f476e491d3acef78' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set a header on the Response.
     *
     * @param  string  $key
     * @param  array|string  $values
     * @param  bool  $replace
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'header',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd8b3d962dbae0cd6c98b231aad02c2fe' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add an array of headers to the response.
     *
     * @param  \\Symfony\\Component\\HttpFoundation\\HeaderBag|array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'withHeaders',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6f5a78293661ceed208d54b8f5225343' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add a cookie to the response.
     *
     * @param  \\Symfony\\Component\\HttpFoundation\\Cookie|mixed  $cookie
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'cookie',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '204fdfadb0531887b19a4f4f6fd66b76' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add a cookie to the response.
     *
     * @param  \\Symfony\\Component\\HttpFoundation\\Cookie|mixed  $cookie
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'withCookie',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bba10f03155792d208e05d8b4ca86b26' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Expire a cookie when sending the response.
     *
     * @param  \\Symfony\\Component\\HttpFoundation\\Cookie|mixed  $cookie
     * @param  string|null  $path
     * @param  string|null  $domain
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'withoutCookie',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '32c4b9416c601509e87ce0d848a5adfd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the callback of the response.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'getCallback',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '62fd1b85263893ce2086198ba46bd157' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the exception to attach to the response.
     *
     * @param  \\Throwable  $e
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'withException',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '572ff0686340283b4cff6ac98ea103c5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Throws the response in a HttpResponseException instance.
     *
     * @return void
     *
     * @throws \\Illuminate\\Http\\Exceptions\\HttpResponseException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'httpresponseexception' => 'Illuminate\\Http\\Exceptions\\HttpResponseException',
          'headerbag' => 'Symfony\\Component\\HttpFoundation\\HeaderBag',
          'throwable' => 'Throwable',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'throwResponse',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e3a58cb4102d6fea0652b71ee6e8d8d1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The registered string macros.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8239dc53dc715696753605864d9f8e75' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a custom macro.
     *
     * @param  string  $name
     * @param  object|callable  $macro
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'macro',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dcbb5b92663e9bdcb651275d38c65155' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Mix another object into the class.
     *
     * @param  object  $mixin
     * @param  bool  $replace
     * @return void
     *
     * @throws \\ReflectionException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'mixin',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5a8d09a03ae90ea8154bcf8ae4c848c7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Checks if macro is registered.
     *
     * @param  string  $name
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'hasMacro',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1b5bfa3f386412b563cbe7c241a24bc7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically handle calls to the class.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => '__callStatic',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'acc6ee2b7636be9bec7e06323c72c980' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically handle calls to the class.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'closure' => 'Closure',
          'reflectionclass' => 'ReflectionClass',
          'reflectionmethod' => 'ReflectionMethod',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'macroCall',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '893ce1be4cadd3ea8387e37f0e21c9a1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Constructor.
     *
     * @param  mixed  $data
     * @param  int  $status
     * @param  array  $headers
     * @param  int  $options
     * @param  bool  $json
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => '__construct',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fff9a6b84959d144981ceb225d4972dd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * {@inheritdoc}
     *
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'fromJsonString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '07b26144edde1acd58dfca3600617fc4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Sets the JSONP callback.
     *
     * @param  string|null  $callback
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'withCallback',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'eb809b6057a9bb2fe266d59946dfd03e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the json_decoded data from the response.
     *
     * @param  bool  $assoc
     * @param  int  $depth
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'getData',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fc02565b01e95fbed9bbeae8dd253611' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * {@inheritdoc}
     *
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'setData',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a194c972eddef3d917096691c27e771a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if an error occurred during JSON encoding.
     *
     * @param  int  $jsonError
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'hasValidJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7ef9ef8c9a5417c9a75447643b50a090' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * {@inheritdoc}
     *
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'setEncodingOptions',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd35ae7152febeafc477fa8e292fd98df' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if a JSON encoding option is set.
     *
     * @param  int  $option
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Http',
         'uses' => 
        array (
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'macroable' => 'Illuminate\\Support\\Traits\\Macroable',
          'invalidargumentexception' => 'InvalidArgumentException',
          'jsonserializable' => 'JsonSerializable',
          'basejsonresponse' => 'Symfony\\Component\\HttpFoundation\\JsonResponse',
        ),
         'className' => 'Illuminate\\Http\\JsonResponse',
         'functionName' => 'hasEncodingOption',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));