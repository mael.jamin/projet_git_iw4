<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/vendor/phpunit/phpunit/src/Framework/Assert.php-1632555531',
   'data' => 
  array (
    '4e61abd0e7c716acfcf29c12cfdf797d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
 * @no-named-arguments Parameter names are not covered by the backward compatibility promise for PHPUnit
 */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a0069a5d7a6e2324f5250e06310847d1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @var int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dcb067a776ac7f57c732c8d1baa0fe48' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that an array has a specified key.
     *
     * @param int|string        $key
     * @param array|ArrayAccess $array
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertArrayHasKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '89bdd838741872eafa96c141807cabbb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that an array does not have a specified key.
     *
     * @param int|string        $key
     * @param array|ArrayAccess $array
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertArrayNotHasKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4bdc38adf81d88a41e3c754616a0032c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a haystack contains a needle.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertContains',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'afa46c95dfda0a213e7887cfe1c33615' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a haystack does not contain a needle.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotContains',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fa4132a52c2c2a7d69073e8d02751b28' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a haystack contains only values of a given type.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertContainsOnly',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3bdafa5d38d3e865d8c4116bd6ac13c2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a haystack contains only instances of a given class name.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertContainsOnlyInstancesOf',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a858af5579442f89ce8382084163fbfa' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a haystack does not contain only values of a given type.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotContainsOnly',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bf62c303d38e45f9727d5e51de60e58d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts the number of elements of an array, Countable or Traversable.
     *
     * @param Countable|iterable $haystack
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1910d10b5dc1078dda69882318458dcb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts the number of elements of an array, Countable or Traversable.
     *
     * @param Countable|iterable $haystack
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bbc18a00760ff7bb64dd5942530cb870' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertEquals',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a771e3600ca62d36fe3cb37fed769772' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are equal (canonicalizing).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertEqualsCanonicalizing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '74ab552c10cde1738097bc586c0a85ae' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are equal (ignoring case).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertEqualsIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '04bceae4e5e665ef1ea251fccd7676f0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are equal (with delta).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertEqualsWithDelta',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c279460de350e59f88dfa3ad7ce7782e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are not equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotEquals',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3f4357a2aedd5000043f809f6d78fd2c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are not equal (canonicalizing).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotEqualsCanonicalizing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1da57e924af7a64b47ed7a6986f8a6c0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are not equal (ignoring case).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotEqualsIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '689d3d4dc19e5f2fd116ef1f0ba69d78' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables are not equal (with delta).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotEqualsWithDelta',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '25bccea1f48e3e8ba208ed4416029dc4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertObjectEquals',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd25a2c1a8f7b0661a0f3e991df49efdf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is empty.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert empty $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertEmpty',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '12c4ce1029e0873b0ad46df24d492e60' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not empty.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !empty $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotEmpty',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e03176e6b7588df8571875e97a7401eb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a value is greater than another value.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertGreaterThan',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e563e02a52658c150a6d9b9222f267c3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a value is greater than or equal to another value.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertGreaterThanOrEqual',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5ade0c8f3b8e8ca0ddf4d86d4b221f06' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a value is smaller than another value.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertLessThan',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dbb85ebe361b6031f3011bb0b49690a2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a value is smaller than or equal to another value.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertLessThanOrEqual',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '34209b7ff8da886d0f2c1a0d5ee92a71' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of one file is equal to the contents of another
     * file.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileEquals',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'edd51a3652b29381aa507975cbdf4954' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of one file is equal to the contents of another
     * file (canonicalizing).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileEqualsCanonicalizing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '32d128302bf2194a4b5106a65e6b7686' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of one file is equal to the contents of another
     * file (ignoring case).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileEqualsIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '02d09ef169c40149ffc031c259978c6f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of one file is not equal to the contents of
     * another file.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileNotEquals',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e99a59b7231700eb2f63dcd30dca2449' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of one file is not equal to the contents of another
     * file (canonicalizing).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileNotEqualsCanonicalizing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '576f5ec1281717a2011436b7b58d7c5c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of one file is not equal to the contents of another
     * file (ignoring case).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileNotEqualsIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6e2e566af95958901d6738eafcf79219' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of a string is equal
     * to the contents of a file.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringEqualsFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '297521508cbc9360ca95b45ec13f3de7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of a string is equal
     * to the contents of a file (canonicalizing).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringEqualsFileCanonicalizing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9490ca096e232b7fceb56fc1a81c13c1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of a string is equal
     * to the contents of a file (ignoring case).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringEqualsFileIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '43681dedddcb0270412dd26bdb098e9a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of a string is not equal
     * to the contents of a file.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotEqualsFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5bded1fde005ea0466180a5ceac6bbda' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of a string is not equal
     * to the contents of a file (canonicalizing).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotEqualsFileCanonicalizing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'acbc6434205911986ff1dbdb0e2a9499' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the contents of a string is not equal
     * to the contents of a file (ignoring case).
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotEqualsFileIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f6cc259df04e656c1dfac80b53696617' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file/dir is readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8015525a86f2aae4d76d99d7617ad6e2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file/dir exists and is not readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '30a7fb9aba03751a142cd63a33d49918' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file/dir exists and is not readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4062
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotIsReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '850f2d6d4dee5cf754557d5d49029527' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file/dir exists and is writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1a426fe365a9931540b3b87d874e85b3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file/dir exists and is not writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b0698c9665753d18318d2647a4d29f88' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file/dir exists and is not writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4065
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotIsWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3effccd97c476829ec57331e19a9dda0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '13019bb0bed0ef37dde6c06df7c401d1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory does not exist.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryDoesNotExist',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae81427ffed4f5517fd17ce775c2daa4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory does not exist.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4068
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryNotExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6be5908798848ef8f2144edc0ed246f3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists and is readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryIsReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f550661b821ab8ff5df3d452d79bf8cf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists and is not readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryIsNotReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fea74dab7fe95f7b1879c68ff487dad0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists and is not readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4071
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryNotIsReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1263be5f49331ecaf821fd7e45b47bfd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists and is writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryIsWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bc4aa7027b185ca0aa7033a75e6f1741' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists and is not writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryIsNotWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '19871d2b7cca0459cbf304e9994a8f3d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a directory exists and is not writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4074
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDirectoryNotIsWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ffaa94d2d29428d27fdf9e025f916f72' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f8436649ace766870bf49b1d74677b6d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file does not exist.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileDoesNotExist',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '26e4edf4f744fa7a10b57c1468e61a67' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file does not exist.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4077
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileNotExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '877a6c40a892c6bb6c71031c0e60a174' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists and is readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileIsReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '57b5b4cd701ab9ad50188f038b768a48' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists and is not readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileIsNotReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '45ef5e12c8011d8b570d863e584861ae' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists and is not readable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4080
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileNotIsReadable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c0ea612de6af1da3e348415149f8b324' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists and is writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileIsWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '356950770a1914187ee5d9499dfa2ac8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists and is not writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileIsNotWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3f85fcaa2cc187ea662b4db5b24da85d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a file exists and is not writable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4083
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFileNotIsWritable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '01ad90398f5571e60724d9d141bd939e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a condition is true.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert true $condition
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertTrue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '75f22f943e8ff4e7d13258322b39e706' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a condition is not true.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !true $condition
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotTrue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '66730317fffbe43cefb0d834e05a1d24' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a condition is false.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert false $condition
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFalse',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dc42e512d5d29ff9551364a44f078f5a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a condition is not false.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !false $condition
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotFalse',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '79c7c88a346c009af021685597538432' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is null.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert null $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNull',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cd9ce6781508e47cffee25cc1323f26d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not null.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !null $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotNull',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '187fc3d82260654e334553559246a4d8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is finite.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertFinite',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2a1275ac46650e95ae69a75b51976f1b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is infinite.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertInfinite',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7c3ef7e2d5815b1e237078df4adaa8ec' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is nan.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNan',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '00da4c24e9567317cfaa01f13b9a76ef' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a class has a specified attribute.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertClassHasAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '60a8b3f35bad379550470e3b54ac9205' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a class does not have a specified attribute.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertClassNotHasAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a68f5779519f98a8f2170697d688eb16' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a class has a specified static attribute.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertClassHasStaticAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '825109b6a65df282280b4340bc5a2812' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a class does not have a specified static attribute.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertClassNotHasStaticAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8099ff38aca293c5112b9888861d3c3a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that an object has a specified attribute.
     *
     * @param object $object
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertObjectHasAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'af2d8df5dee9c68c1e65af4c55d5e9e9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that an object does not have a specified attribute.
     *
     * @param object $object
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertObjectNotHasAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5d40f1e6f2661092f6b533525604bc51' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables have the same type and value.
     * Used on objects, it asserts that two variables reference
     * the same object.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-template ExpectedType
     * @psalm-param ExpectedType $expected
     * @psalm-assert =ExpectedType $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertSame',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e5aa123d00078e176ac495d147179e88' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two variables do not have the same type and value.
     * Used on objects, it asserts that two variables do not reference
     * the same object.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotSame',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b6b1a05f278b58c495e3199317050876' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of a given type.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     *
     * @psalm-template ExpectedType of object
     * @psalm-param class-string<ExpectedType> $expected
     * @psalm-assert =ExpectedType $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertInstanceOf',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1fdd70e1961a4a626d9da8c1d394cb23' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of a given type.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     *
     * @psalm-template ExpectedType of object
     * @psalm-param class-string<ExpectedType> $expected
     * @psalm-assert !ExpectedType $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotInstanceOf',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ee48c6ae23442edc7391ac519cf56cec' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type array.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert array $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f83c30bbde338fb5f9a842ea95775e35' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type bool.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert bool $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsBool',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e2cb9fd10144e65685b71275149a4458' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type float.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert float $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsFloat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0746870b052471244182faf58e642762' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type int.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert int $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsInt',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0529a1f539d8fcb25836cd882698b741' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type numeric.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert numeric $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNumeric',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd32ee85e73f9196e516495417d8e70cb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type object.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert object $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsObject',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a618c4f16f4d0d6c6158460b2c2616d4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type resource.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert resource $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsResource',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7716fc7b91fb7d4742ad2e20c686dacf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type resource and is closed.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert resource $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsClosedResource',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a9458d985431ed455487f378357359f8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type string.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert string $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c85c6d84d6db708f2f5ef9d2c81b18db' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type scalar.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert scalar $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsScalar',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '702fa98f02fae2234dd4ae3b1b4cd23a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type callable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert callable $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsCallable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '09aeea689a8bb5faa7a2de5ef4f14566' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is of type iterable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert iterable $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsIterable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9d386f45644345dfd4030734f4fa55a0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type array.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !array $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6943cb02faf839e3c6fd1b79556fc146' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type bool.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !bool $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotBool',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a9e3aef9f7d0986a40ca8b6787725045' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type float.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !float $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotFloat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3e010c681195e14890248f2c39e6991e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type int.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !int $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotInt',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '80f92f021e396fe8e4ca5e94c11c183e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type numeric.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !numeric $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotNumeric',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9e2704131c69ad9305c7242a64e511c7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type object.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !object $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotObject',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8177080e6f2fd5559f02b4eab7c20bdc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type resource.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !resource $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotResource',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '523b5e73c7bd9617c463ee105757ca13' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type resource.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !resource $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotClosedResource',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '290aa42833d2c5bdb136cf7c6cc90f70' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type string.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !string $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '624cbc86120671d6380b70343e93396a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type scalar.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !scalar $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotScalar',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '98c568c5a1756c5b04a61b14d724bf41' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type callable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !callable $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotCallable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6329b02d67f5ff15c4d3b07ff165ae9b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a variable is not of type iterable.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @psalm-assert !iterable $actual
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertIsNotIterable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7a5d5416f740c4a9a277118b162e2213' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string matches a given regular expression.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertMatchesRegularExpression',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '20f61ee0bbf43a436a7c901ddb2ff4bb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string matches a given regular expression.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4086
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertRegExp',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0111f114b5c3a62873fd12dfaaabfc1a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string does not match a given regular expression.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertDoesNotMatchRegularExpression',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '282c0ef7f2fed56b6bffa5f5570a711d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string does not match a given regular expression.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4089
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotRegExp',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ce28933b7e110ca85553aa1493999aed' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the size of two arrays (or `Countable` or `Traversable` objects)
     * is the same.
     *
     * @param Countable|iterable $expected
     * @param Countable|iterable $actual
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertSameSize',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c3b417558b5dc4ae1612e55cc517a4e0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the size of two arrays (or `Countable` or `Traversable` objects)
     * is not the same.
     *
     * @param Countable|iterable $expected
     * @param Countable|iterable $actual
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertNotSameSize',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5bc7814c52e007118b8bcdf8776864e7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string matches a given format string.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringMatchesFormat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '18a3fa5a2bb4f96cf98469974ae69736' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string does not match a given format string.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotMatchesFormat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd396d0356b683b8499be0ed3d9e3b1ca' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string matches a given format file.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringMatchesFormatFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2e607da05858137c294f457969e745a1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string does not match a given format string.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotMatchesFormatFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '80f9c3be504d87180893279ad30ed47c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string starts with a given prefix.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringStartsWith',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '664c5fbb87b753fd834c90732f5f6ce8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string starts not with a given prefix.
     *
     * @param string $prefix
     * @param string $string
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringStartsNotWith',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c0e29edd5e2093774daca73727181bfe' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringContainsString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '14315677e45742322be57760744f0c12' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringContainsStringIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '29fdb8eee460e67367c406a163b2f93a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotContainsString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '28d1249f2daba7c5e594b809e0f67685' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringNotContainsStringIgnoringCase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5fd41c2dcee763f307097a7bbed5c662' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string ends with a given suffix.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringEndsWith',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '332479b993e39635d89c639b414441d5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string ends not with a given suffix.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertStringEndsNotWith',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '266cda0b34754c6dfe980aa2259b15f8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two XML files are equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws Exception
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertXmlFileEqualsXmlFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '61af5f72b7abae96ee17aaade6886fa0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two XML files are not equal.
     *
     * @throws \\PHPUnit\\Util\\Exception
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertXmlFileNotEqualsXmlFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1967da2613e34b9ff784b24ca95dad43' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two XML documents are equal.
     *
     * @param DOMDocument|string $actualXml
     *
     * @throws \\PHPUnit\\Util\\Xml\\Exception
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertXmlStringEqualsXmlFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b86a73f9cb01a2cf106e37e040bf5a77' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two XML documents are not equal.
     *
     * @param DOMDocument|string $actualXml
     *
     * @throws \\PHPUnit\\Util\\Xml\\Exception
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertXmlStringNotEqualsXmlFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '476ff00d624fd07172b204230d855cf0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two XML documents are equal.
     *
     * @param DOMDocument|string $expectedXml
     * @param DOMDocument|string $actualXml
     *
     * @throws \\PHPUnit\\Util\\Xml\\Exception
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertXmlStringEqualsXmlString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c0014ceed9b129ae434e867595139254' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two XML documents are not equal.
     *
     * @param DOMDocument|string $expectedXml
     * @param DOMDocument|string $actualXml
     *
     * @throws \\PHPUnit\\Util\\Xml\\Exception
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertXmlStringNotEqualsXmlString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b38f31365f94e9f592e5e9d895cc18f7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a hierarchy of DOMElements matches.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws AssertionFailedError
     * @throws ExpectationFailedException
     *
     * @codeCoverageIgnore
     *
     * @deprecated https://github.com/sebastianbergmann/phpunit/issues/4091
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertEqualXMLStructure',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '99d8b9bd4b1fbaa30f3ef9a9928355bf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Evaluates a PHPUnit\\Framework\\Constraint matcher object.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertThat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd59451abbe16e061ed050af449066446' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that a string is a valid JSON string.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '676e1be40f9e223b65881f033115bc4f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two given JSON encoded objects or arrays are equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJsonStringEqualsJsonString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8c88c9c1acc31d9ac726b10c6148299b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two given JSON encoded objects or arrays are not equal.
     *
     * @param string $expectedJson
     * @param string $actualJson
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJsonStringNotEqualsJsonString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '40f8c16ca97e7d88b0d3a28170d41b8c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the generated JSON encoded object and the content of the given file are equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJsonStringEqualsJsonFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '311e5b48220c006045e594ab6aff5a9a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the generated JSON encoded object and the content of the given file are not equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJsonStringNotEqualsJsonFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '17051610400d569bb28e86974fd5ff34' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two JSON files are equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJsonFileEqualsJsonFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '25cf92dd5145254583f7d0367e3a81bc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that two JSON files are not equal.
     *
     * @throws \\SebastianBergmann\\RecursionContext\\InvalidArgumentException
     * @throws ExpectationFailedException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'assertJsonFileNotEqualsJsonFile',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae75686e9cfa1f0be47671502696a665' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @throws Exception
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'logicalAnd',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '77e907d6cfd26ef46db5c599e00cbafe' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @psalm-template CallbackInput of mixed
     *
     * @psalm-param callable(CallbackInput $callback): bool $callback
     *
     * @psalm-return Callback<CallbackInput>
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'callback',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '306f6f70f89fc335ddbe73242bd04b73' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @param int|string $key
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'arrayHasKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '56ac436f0105c8d593db3721e37bc6af' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Fails a test with the given message.
     *
     * @throws AssertionFailedError
     *
     * @psalm-return never-return
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'fail',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9a41dde3e02fcaed37ba32223802f3a5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Mark the test as incomplete.
     *
     * @throws IncompleteTestError
     *
     * @psalm-return never-return
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'markTestIncomplete',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cebbee75a9feca188b3ea121b4302ccc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Mark the test as skipped.
     *
     * @throws SkippedTestError
     * @throws SyntheticSkippedError
     *
     * @psalm-return never-return
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'markTestSkipped',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9cd4c781d3c7ca9a1e9ac26d1375d140' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Return the current assertion count.
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'getCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '186f29e0e98bb8da5ab671f7442e25a2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Reset the assertion counter.
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'resetCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '21fbc851261164022e8ac3891d275099' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * @codeCoverageIgnore
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'PHPUnit\\Framework',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'countable' => 'Countable',
          'domattr' => 'DOMAttr',
          'domdocument' => 'DOMDocument',
          'domelement' => 'DOMElement',
          'arrayhaskey' => 'PHPUnit\\Framework\\Constraint\\ArrayHasKey',
          'callback' => 'PHPUnit\\Framework\\Constraint\\Callback',
          'classhasattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasAttribute',
          'classhasstaticattribute' => 'PHPUnit\\Framework\\Constraint\\ClassHasStaticAttribute',
          'constraint' => 'PHPUnit\\Framework\\Constraint\\Constraint',
          'count' => 'PHPUnit\\Framework\\Constraint\\Count',
          'directoryexists' => 'PHPUnit\\Framework\\Constraint\\DirectoryExists',
          'fileexists' => 'PHPUnit\\Framework\\Constraint\\FileExists',
          'greaterthan' => 'PHPUnit\\Framework\\Constraint\\GreaterThan',
          'isanything' => 'PHPUnit\\Framework\\Constraint\\IsAnything',
          'isempty' => 'PHPUnit\\Framework\\Constraint\\IsEmpty',
          'isequal' => 'PHPUnit\\Framework\\Constraint\\IsEqual',
          'isequalcanonicalizing' => 'PHPUnit\\Framework\\Constraint\\IsEqualCanonicalizing',
          'isequalignoringcase' => 'PHPUnit\\Framework\\Constraint\\IsEqualIgnoringCase',
          'isequalwithdelta' => 'PHPUnit\\Framework\\Constraint\\IsEqualWithDelta',
          'isfalse' => 'PHPUnit\\Framework\\Constraint\\IsFalse',
          'isfinite' => 'PHPUnit\\Framework\\Constraint\\IsFinite',
          'isidentical' => 'PHPUnit\\Framework\\Constraint\\IsIdentical',
          'isinfinite' => 'PHPUnit\\Framework\\Constraint\\IsInfinite',
          'isinstanceof' => 'PHPUnit\\Framework\\Constraint\\IsInstanceOf',
          'isjson' => 'PHPUnit\\Framework\\Constraint\\IsJson',
          'isnan' => 'PHPUnit\\Framework\\Constraint\\IsNan',
          'isnull' => 'PHPUnit\\Framework\\Constraint\\IsNull',
          'isreadable' => 'PHPUnit\\Framework\\Constraint\\IsReadable',
          'istrue' => 'PHPUnit\\Framework\\Constraint\\IsTrue',
          'istype' => 'PHPUnit\\Framework\\Constraint\\IsType',
          'iswritable' => 'PHPUnit\\Framework\\Constraint\\IsWritable',
          'jsonmatches' => 'PHPUnit\\Framework\\Constraint\\JsonMatches',
          'lessthan' => 'PHPUnit\\Framework\\Constraint\\LessThan',
          'logicaland' => 'PHPUnit\\Framework\\Constraint\\LogicalAnd',
          'logicalnot' => 'PHPUnit\\Framework\\Constraint\\LogicalNot',
          'logicalor' => 'PHPUnit\\Framework\\Constraint\\LogicalOr',
          'logicalxor' => 'PHPUnit\\Framework\\Constraint\\LogicalXor',
          'objectequals' => 'PHPUnit\\Framework\\Constraint\\ObjectEquals',
          'objecthasattribute' => 'PHPUnit\\Framework\\Constraint\\ObjectHasAttribute',
          'regularexpression' => 'PHPUnit\\Framework\\Constraint\\RegularExpression',
          'samesize' => 'PHPUnit\\Framework\\Constraint\\SameSize',
          'stringcontains' => 'PHPUnit\\Framework\\Constraint\\StringContains',
          'stringendswith' => 'PHPUnit\\Framework\\Constraint\\StringEndsWith',
          'stringmatchesformatdescription' => 'PHPUnit\\Framework\\Constraint\\StringMatchesFormatDescription',
          'stringstartswith' => 'PHPUnit\\Framework\\Constraint\\StringStartsWith',
          'traversablecontainsequal' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsEqual',
          'traversablecontainsidentical' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsIdentical',
          'traversablecontainsonly' => 'PHPUnit\\Framework\\Constraint\\TraversableContainsOnly',
          'type' => 'PHPUnit\\Util\\Type',
          'xml' => 'PHPUnit\\Util\\Xml',
          'xmlloader' => 'PHPUnit\\Util\\Xml\\Loader',
        ),
         'className' => 'PHPUnit\\Framework\\Assert',
         'functionName' => 'createWarning',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));