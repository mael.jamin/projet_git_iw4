<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/vendor/laravel/lumen-framework/src/Console/Kernel.php-1630686510',
   'data' => 
  array (
    '5e7f41f3fdaa44822c244b83bb61ae20' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The application implementation.
     *
     * @var \\Laravel\\Lumen\\Application
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5a9fed12ba9abf645ccb3c758e4e775b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The Artisan application instance.
     *
     * @var \\Illuminate\\Console\\Application
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '785646f32cd008fe9bac75dfa9d9ff1f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if facade aliases are enabled for the console.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5e3e52615ac637b9a456d205662a0af2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The Artisan commands provided by the application.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9a69842b62cc048cdadc0a833cff870a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new console kernel instance.
     *
     * @param  \\Laravel\\Lumen\\Application  $app
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => '__construct',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '88df9d66b864e42c87b1224b71def190' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the request instance for URL generation.
     *
     * @param  \\Illuminate\\Contracts\\Foundation\\Application  $app
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'setRequestForConsole',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '35625081974f202a85208910d84a0d4a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define the application\'s command schedule.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'defineConsoleSchedule',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '512170de1aa7cc8e9965624ba502f588' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Run the console application.
     *
     * @param  \\Symfony\\Component\\Console\\Input\\InputInterface  $input
     * @param  \\Symfony\\Component\\Console\\Output\\OutputInterface  $output
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'handle',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba7e7abe1ecb1e3452e898c601fdd167' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Bootstrap the application for artisan commands.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'bootstrap',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cc4c4f017e21b90e734f9b3acf56a70f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Terminate the application.
     *
     * @param  \\Symfony\\Component\\Console\\Input\\InputInterface  $input
     * @param  int  $status
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'terminate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9a575d0b82322b394a57dd7097e2bf58' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define the application\'s command schedule.
     *
     * @param  \\Illuminate\\Console\\Scheduling\\Schedule  $schedule
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'schedule',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3ae3cdd40da7d4aba3a17547c2c8619a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Run an Artisan console command by name.
     *
     * @param  string  $command
     * @param  array  $parameters
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'call',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '475a5394e2808d585ed79db195250e92' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Queue the given console command.
     *
     * @param  string  $command
     * @param  array  $parameters
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'queue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '780196c1633a1088b8e33fea55bcc623' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all of the commands registered with the console.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'all',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2ef2f397ce22a7e3ad5c64b581cdcd0f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the output for the last run command.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'output',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e1a890528a72203995d9e1be257d0531' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the Artisan application instance.
     *
     * @return \\Illuminate\\Console\\Application
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'getArtisan',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fb9c57343ba63336ed8dad623c239dc4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the commands to add to the application.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'getCommands',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '34ea9e7b4b294f388dbd7f140f612f1f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Report the exception to the exception handler.
     *
     * @param  \\Throwable  $e
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'reportException',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'df4c79191c396f59637558df410b5f97' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Report the exception to the exception handler.
     *
     * @param  \\Symfony\\Component\\Console\\Output\\OutputInterface  $output
     * @param  \\Throwable  $e
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'renderException',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f5e4b01643a1d2cf7e5e1422fdafed6c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the exception handler from the container.
     *
     * @return \\Illuminate\\Contracts\\Debug\\ExceptionHandler
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Console',
         'uses' => 
        array (
          'artisan' => 'Illuminate\\Console\\Application',
          'schedule' => 'Illuminate\\Console\\Scheduling\\Schedule',
          'scheduleruncommand' => 'Illuminate\\Console\\Scheduling\\ScheduleRunCommand',
          'kernelcontract' => 'Illuminate\\Contracts\\Console\\Kernel',
          'exceptionhandler' => 'Illuminate\\Contracts\\Debug\\ExceptionHandler',
          'request' => 'Illuminate\\Http\\Request',
          'application' => 'Laravel\\Lumen\\Application',
          'handler' => 'Laravel\\Lumen\\Exceptions\\Handler',
          'runtimeexception' => 'RuntimeException',
          'throwable' => 'Throwable',
        ),
         'className' => 'Laravel\\Lumen\\Console\\Kernel',
         'functionName' => 'resolveExceptionHandler',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));