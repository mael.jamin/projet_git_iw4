<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/vendor/laravel/lumen-framework/src/Testing/TestCase.php-1630686510,/var/www/html/vendor/laravel/lumen-framework/src/Testing/Concerns/MakesHttpRequests.php-1630686510',
   'data' => 
  array (
    'd57bc33b790ae3878a342dc781c072a6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The last response returned by the application.
     *
     * @var \\Illuminate\\Testing\\TestResponse
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b5b4221303a0d855f54001b562aaaa3a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The current URI being viewed.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '36533996bec84024fbd09cfbf31326ed' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a JSON request.
     *
     * @param  string  $method
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'json',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2181e519818e8c4f0b5ba2edbea36f46' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a GET request.
     *
     * @param  string  $uri
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'get',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9580daef0b3a5f254027e17d26e338a1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a POST request.
     *
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'post',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '14433bf1f8f50947fca88599a3c1f0fb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a PUT request.
     *
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'put',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '34b7585cec5c3811b522d8420a42d5d9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a PATCH request.
     *
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'patch',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '39d45291084223c0bfa5843d2366d8bd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a DELETE request.
     *
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'delete',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e75e46b634b3cd8beb674116ff3238ad' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a OPTIONS request.
     *
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'options',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9d9a074dc689d12cf4de35b0aac8be0a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Visit the given URI with a HEAD request.
     *
     * @param  string  $uri
     * @param  array  $data
     * @param  array  $headers
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'head',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3f6bbe15c8d732d55c343c6e1b305e22' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Send the given request through the application.
     *
     * This method allows you to fully customize the entire Request object.
     *
     * @param  \\Illuminate\\Http\\Request  $request
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'handle',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '41043228867214297e880ecf91f1b3f0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response contains JSON.
     *
     * @param  array|null  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'shouldReturnJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '84c280496281eb13ef29892e3485b43a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response contains JSON.
     *
     * @param  array|null  $data
     * @return $this|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'receiveJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '976a36200dd6a04ba3acbb37e385bf9e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response contains an exact JSON array.
     *
     * @param  array  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeJsonEquals',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6624ae7643b6e454f7e39fb1fd4c0a02' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response contains JSON.
     *
     * @param  array|null  $data
     * @param  bool  $negate
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '24bca70e938cb57494c554f49a757106' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response doesn\'t contain JSON.
     *
     * @param  array|null  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'dontSeeJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '490162313d2fe30935df8627d88c60b1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the JSON response has a given structure.
     *
     * @param  array|null  $structure
     * @param  array|null  $responseData
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeJsonStructure',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '305e0871970e98ef0dd509c281fb678e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response contains the given JSON.
     *
     * @param  array  $data
     * @param  bool  $negate
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeJsonContains',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0431899dee73b4819d80e931e17f5a5e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the response doesn\'t contain the given JSON.
     *
     * @param  array  $data
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeJsonDoesntContains',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a976c198223c87c9be2063129b0a5181' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Format the given key and value into a JSON string for expectation checks.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'formatToExpectedJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b998833f1dc751d50cfc1f3a2a3c7c71' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Call the given URI and return the Response.
     *
     * @param  string  $method
     * @param  string  $uri
     * @param  array  $parameters
     * @param  array  $cookies
     * @param  array  $files
     * @param  array  $server
     * @param  string  $content
     * @return \\Illuminate\\Http\\Response
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'call',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '39bb6fe3aa03cebfa3182c281b92ec5d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Turn the given URI into a fully qualified URL.
     *
     * @param  string  $uri
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'prepareUrlForRequest',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fd647d5eb7234f57aa3f3f84578c8bf7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Transform headers array to array of $_SERVER vars with HTTP_* format.
     *
     * @param  array  $headers
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'transformHeadersToServerVars',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '67535c3b7136ac071b01e47e1ddc1c14' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the client response has an OK status code.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'assertResponseOk',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '60b9c4be2bc164d7f389caa77e773967' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that the client response has a given status code.
     *
     * @param  int  $status
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'assertResponseStatus',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '30629a9a57f773ad3ed5d5a5b727061c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the status code of the response matches the given code.
     *
     * @param  int  $status
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeStatusCode',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9284c7d5f1aa74e948e2e36c4172f4d5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Asserts that the response contains the given header and equals the optional value.
     *
     * @param  string  $headerName
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeHeader',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '17e3dd4e49ad17b763149372a2dcad09' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Disable middleware for the test.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing\\Concerns',
         'uses' => 
        array (
          'request' => 'Illuminate\\Http\\Request',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
          'phpunit' => 'Illuminate\\Testing\\Assert',
          'testresponse' => 'Illuminate\\Testing\\TestResponse',
          'lumenrequest' => 'Laravel\\Lumen\\Http\\Request',
          'symfonyrequest' => 'Symfony\\Component\\HttpFoundation\\Request',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'withoutMiddleware',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd21a09c4cf87fccbeb81b9b02a1c5358' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The application instance.
     *
     * @var \\Laravel\\Lumen\\Application
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '45756de76334c914210a39f4a07a6fd8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The base URL to use while testing the application.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd4b19e3e97b1014db504bb72549baafe' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The callbacks that should be run before the application is destroyed.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bf10eee460046e3c808c8a82419b9e31' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return \\Symfony\\Component\\HttpKernel\\HttpKernelInterface
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'createApplication',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6f77f9aae172239865355ff2f71b7508' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Refresh the application instance.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'refreshApplication',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5634f83e9aaa192d08065abc47329b26' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Setup the test environment.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'setUp',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cb9e9ee20b9fa788acc449152d977296' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Boot the testing helper traits.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'setUpTraits',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '28ff4fc8265861997dcfdd65a37ddab2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'tearDown',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7a6c5fd9b8571f91fb6a2c117b569c1e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that a given where condition exists in the database.
     *
     * @param  string  $table
     * @param  array  $data
     * @param  string|null $onConnection
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'seeInDatabase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2412e255bc6994af46a988b61cf2aafd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that a given where condition does not exist in the database.
     *
     * @param  string  $table
     * @param  array  $data
     * @param  string|null $onConnection
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'missingFromDatabase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '82caf98430869200d8558f06beafa65a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Assert that a given where condition does not exist in the database.
     *
     * @param  string  $table
     * @param  array  $data
     * @param  string|null $onConnection
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'notSeeInDatabase',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a56ccf740197ec93abad4d7fca65dbb9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Specify a list of events that should be fired for the given operation.
     *
     * These events will be mocked, so that handlers will not actually be executed.
     *
     * @param  array|string  $events
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'expectsEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2f52381ff4394837ab79c40f6895cd9c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Mock the event dispatcher so all events are silenced.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'withoutEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '601dc4d5151da5256fe520b45448d961' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Specify a list of jobs that should be dispatched for the given operation.
     *
     * These jobs will be mocked, so that handlers will not actually be executed.
     *
     * @param  array|string  $jobs
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'expectsJobs',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8ccc9b57982e4df6928e385c685f59cb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Mock the job dispatcher so all jobs are silenced and collected.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'withoutJobs',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7d8bef51f0f18e337b30a31be8f1b73e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the currently logged in user for the application.
     *
     * @param  \\Illuminate\\Contracts\\Auth\\Authenticatable  $user
     * @param  string|null  $driver
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'actingAs',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '59d8e12b672cbf7b4c72ea9b2dc3c9ca' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the currently logged in user for the application.
     *
     * @param  \\Illuminate\\Contracts\\Auth\\Authenticatable  $user
     * @param  string|null  $driver
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'be',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0c5250c29f0b7f12b51efbd67a243fa9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Call artisan command and return code.
     *
     * @param  string  $command
     * @param  array  $parameters
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'artisan',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '970a862e76e4e19cca35c56531adb822' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a callback to be run before the application is destroyed.
     *
     * @param  callable  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Laravel\\Lumen\\Testing',
         'uses' => 
        array (
          'exception' => 'Exception',
          'authenticatable' => 'Illuminate\\Contracts\\Auth\\Authenticatable',
          'kernel' => 'Illuminate\\Contracts\\Console\\Kernel',
          'facade' => 'Illuminate\\Support\\Facades\\Facade',
          'mockery' => 'Mockery',
          'basetestcase' => 'PHPUnit\\Framework\\TestCase',
        ),
         'className' => 'Laravel\\Lumen\\Testing\\TestCase',
         'functionName' => 'beforeApplicationDestroyed',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));