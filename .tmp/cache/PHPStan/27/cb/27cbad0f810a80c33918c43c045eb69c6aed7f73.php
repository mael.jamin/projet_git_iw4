<?php declare(strict_types = 1);

return PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => '/var/www/html/vendor/illuminate/database/Eloquent/Model.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/HasAttributes.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/HasEvents.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/HasGlobalScopes.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/HasRelationships.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/HasTimestamps.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/HidesAttributes.php-1632834643,/var/www/html/vendor/illuminate/database/Eloquent/Concerns/GuardsAttributes.php-1632834643,/var/www/html/vendor/illuminate/support/Traits/ForwardsCalls.php-1632754679',
   'data' => 
  array (
    '721f075460e4ddabff6c63014d92aac9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The model\'s attributes.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '72b384a86239b58b47e671ac2a9f32d4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The model attribute\'s original state.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e44a837a6015c69c288ad56016745510' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The changed model attributes.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a3e8f4736f2194941f30e3a2ce3e5897' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that should be cast.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6c29e9853b4995b4e6f45f30deb5324a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that have been cast using custom classes.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '34a5e819fc84c8cfc01290325fd085db' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The built-in, primitive cast types supported by Eloquent.
     *
     * @var string[]
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2a728a041f756de4f5645273972da22d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that should be mutated to dates.
     *
     * @deprecated Use the "casts" property
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b8977fe1d39f7f130a47112756fad269' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The storage format of the model\'s date columns.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '42573d59816291b4b9bfd14c6e73d82f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The accessors to append to the model\'s array form.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8f36eead98042637be3746de49fc0f75' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates whether attributes are snake cased on arrays.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '739c47d543620ff750d997d4fd6beba9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The cache of the mutated attributes for each class.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0dd5d50d8f1c9c456d120151349156bd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The encrypter instance that is used to encrypt attributes.
     *
     * @var \\Illuminate\\Contracts\\Encryption\\Encrypter
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6c7700dbbaf7dd80f7abe2c50289d60d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Convert the model\'s attributes to an array.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'attributesToArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3acb2ed679356a38c3d00ad38ea9d6a5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add the date attributes to the attributes array.
     *
     * @param  array  $attributes
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'addDateAttributesToArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ef071c14ccfa527f3ad51f0ed222b6e5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add the mutated attributes to the attributes array.
     *
     * @param  array  $attributes
     * @param  array  $mutatedAttributes
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'addMutatedAttributesToArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f309c7b958e5469bd43192dc54eb1d0c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add the casted attributes to the attributes array.
     *
     * @param  array  $attributes
     * @param  array  $mutatedAttributes
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'addCastAttributesToArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '301eb674ab843a9056a5d556e7225a3e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an attribute array of all arrayable attributes.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getArrayableAttributes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '25d79ada180eb77230adafeb5553ece8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all of the appendable values that are arrayable.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getArrayableAppends',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bd4e266ba47d1e6f0081be3adec75c66' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the model\'s relationships in array form.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'relationsToArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'aad3efa8025fe04e9c04bee6968b2766' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an attribute array of all arrayable relations.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getArrayableRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd92acdd3a90f3c41a000220a3ef8b023' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an attribute array of all arrayable values.
     *
     * @param  array  $values
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getArrayableItems',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f9cfe92c6c5b3a631318d1419b11471d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'def27c1a17b99a986c0fc8e856cba3cd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a plain attribute (not a relationship).
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getAttributeValue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e8bf8b0d612fc3b4645fc65322840ab1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an attribute from the $attributes array.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getAttributeFromArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '48d24debd2a3c8618e8d9624096b3cdb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a relationship.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRelationValue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2c6332a0e26fdcdd14b24f16cf505061' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given key is a relationship method on the model.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isRelation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b6bbe259a7f570004e0dfa2fdd7e0f8a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Handle a lazy loading violation.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'handleLazyLoadingViolation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba822dff938d0d60747ed9519df3fb0e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a relationship value from a method.
     *
     * @param  string  $method
     * @return mixed
     *
     * @throws \\LogicException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRelationshipFromMethod',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4fece1ef8202f1d95b3c10031a37851a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if a get mutator exists for an attribute.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasGetMutator',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '79434d7931c768887f7ce5ace53b4ad9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value of an attribute using its mutator.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'mutateAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b9dab6e3b326be951f74a05c959c8898' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value of an attribute using its mutator for array conversion.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'mutateAttributeForArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9bb3e42f1e81f8facd9f19a2018f6312' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Merge new casts with existing casts on the model.
     *
     * @param  array  $casts
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'mergeCasts',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ce5545c7963d61eec91f9ec6fe439c8c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'castAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6555dbe36b2e9f00675a7cf3eba7456a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Cast the given attribute using a custom cast class.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getClassCastableAttributeValue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0f567b5e5919abfc5c446b0199724eed' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the type of cast for a model attribute.
     *
     * @param  string  $key
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getCastType',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2a1ae31b3e0c2214cb0e81e8d73bc10c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Increment or decrement the given attribute using the custom cast class.
     *
     * @param  string  $method
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'deviateClassCastableAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2a65d79160335eac7fb03884199cead9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Serialize the given attribute using the custom cast class.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'serializeClassCastableAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '773c4183098e9773aa704b170d174b0c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the cast type is a custom date time cast.
     *
     * @param  string  $cast
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isCustomDateTimeCast',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '09f71cd915e096a34e02280c92b70f60' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the cast type is an immutable custom date time cast.
     *
     * @param  string  $cast
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isImmutableCustomDateTimeCast',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'da123cbd16cd697567298c3a808c75b7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the cast type is a decimal cast.
     *
     * @param  string  $cast
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isDecimalCast',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0f6961efc119c9f9e87de9448b30449a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9f52895596dd6d2cca9d0c03bf4e4283' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if a set mutator exists for an attribute.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasSetMutator',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c6563668cc993871f6aee4c491c6dc0e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the value of an attribute using its mutator.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setMutatedAttributeValue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b1fb7632faf47fdc79691c9813a1c915' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given attribute is a date or date castable.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isDateAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e5140a9e9cd673130201ad0587497125' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set a given JSON attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fillJsonAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2b0f4f3b6d939e3c0178bfbcf09a8662' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the value of a class castable attribute.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setClassCastableAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f937a12077faacf8e2459ca3a8b595b4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an array attribute with the given key and value set.
     *
     * @param  string  $path
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getArrayAttributeWithValue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '89ffdf0541e85e76f6acd572ce5cb4bf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get an array attribute or return an empty array if it is not set.
     *
     * @param  string  $key
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getArrayAttributeByKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b41f55c64ffc03dbc76fdc03583a7177' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Cast the given attribute to JSON.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'castAttributeAsJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '67c1952bf6203d07827038b20171eb58' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Encode the given value as JSON.
     *
     * @param  mixed  $value
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'asJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd0f224d25e0509536a5187d3f4b91602' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Decode the given JSON back into an array or object.
     *
     * @param  string  $value
     * @param  bool  $asObject
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fromJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '82df581f4cdf6d620be0051c88c6a9c9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Decrypt the given encrypted string.
     *
     * @param  string  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fromEncryptedString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '469001d80ce1e703d16c1f15d51f175b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Cast the given attribute to an encrypted string.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'castAttributeAsEncryptedString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bfb4fdbdbf5e100cfda6e223eda999c2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the encrypter instance that will be used to encrypt attributes.
     *
     * @param  \\Illuminate\\Contracts\\Encryption\\Encrypter  $encrypter
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'encryptUsing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c636b7dae9ba058ca12ee511354c3759' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Decode the given float.
     *
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fromFloat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f77e60fab583bf8aab0e78c411267a32' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Return a decimal as string.
     *
     * @param  float  $value
     * @param  int  $decimals
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'asDecimal',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2bd7cd04a8f6bd3abe18bd3719427843' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Return a timestamp as DateTime object with time set to 00:00:00.
     *
     * @param  mixed  $value
     * @return \\Illuminate\\Support\\Carbon
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'asDate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8612ed6cbc27f21a29f1af52433d3b95' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Return a timestamp as DateTime object.
     *
     * @param  mixed  $value
     * @return \\Illuminate\\Support\\Carbon
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'asDateTime',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e9a3c9d37b97308b03bdd3bdf413d8bb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given value is a standard date format.
     *
     * @param  string  $value
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isStandardDateFormat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd4f26d3bf9d59852093a917290d725fb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Convert a DateTime to a storable string.
     *
     * @param  mixed  $value
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fromDateTime',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0db19d2240a21d4d7033d1d4dd47a777' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Return a timestamp as unix timestamp.
     *
     * @param  mixed  $value
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'asTimestamp',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cc53926180e27984c2c2513ed653263d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \\DateTimeInterface  $date
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'serializeDate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '39ac037ac71f0c97a906c167486ead5f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the attributes that should be converted to dates.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getDates',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dcb2c42feb8cfdcee22ae9d7f76455b5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the format for database stored dates.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getDateFormat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '827c34a599e3e9898b24753412773212' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the date format used by the model.
     *
     * @param  string  $format
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setDateFormat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1083496b7231bc422afeae950230257c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine whether an attribute should be cast to a native type.
     *
     * @param  string  $key
     * @param  array|string|null  $types
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasCast',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a6e7bd102ed2776fec83757af1c3d055' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the casts array.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getCasts',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ceee4422bbf1fc3c3374600440f139a3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine whether a value is Date / DateTime castable for inbound manipulation.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isDateCastable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '21135eb967f2ed104c548fc3a8ea8787' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine whether a value is Date / DateTime custom-castable for inbound manipulation.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isDateCastableWithCustomFormat',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '71998a54f69c645c1eb91c1cd1eca6bb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine whether a value is JSON castable for inbound manipulation.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isJsonCastable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '12c62dda93b751048871d5f2cccea47f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine whether a value is an encrypted castable for inbound manipulation.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isEncryptedCastable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c46f41e46317da9f9d696ade743965a1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given key is cast using a custom class.
     *
     * @param  string  $key
     * @return bool
     *
     * @throws \\Illuminate\\Database\\Eloquent\\InvalidCastException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isClassCastable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3c9cd947710eba27cb50ca98d984cb88' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the key is deviable using a custom class.
     *
     * @param  string  $key
     * @return bool
     *
     * @throws \\Illuminate\\Database\\Eloquent\\InvalidCastException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isClassDeviable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6c15e34b4afce259919f087517089620' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the key is serializable using a custom class.
     *
     * @param  string  $key
     * @return bool
     *
     * @throws \\Illuminate\\Database\\Eloquent\\InvalidCastException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isClassSerializable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '565b732a3589f8717c8c716f45001ee8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Resolve the custom caster class for a given key.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveCasterClass',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '53a9cfb94f9bd7e61aa2db935827ec34' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Parse the given caster class, removing any arguments.
     *
     * @param  string  $class
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'parseCasterClass',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '20520940e0c5f0c0c277b80730888995' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Merge the cast class attributes back into the model.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'mergeAttributesFromClassCasts',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2c184b9906e922978f4b3d5d222420cc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Normalize the response from a custom class caster.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'normalizeCastClassResponse',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e6f49de3d36d6398283005ab2e35faa9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all of the current attributes on the model.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getAttributes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '345ad24ba7beedb8d87a9b8cdc8f818e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all of the current attributes on the model for an insert operation.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getAttributesForInsert',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '713017880a8b323127f52fb442a20ad2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the array of model attributes. No checking is done.
     *
     * @param  array  $attributes
     * @param  bool  $sync
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setRawAttributes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '66ac75dff7cae0bba71388a4c93ccd69' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the model\'s original attribute values.
     *
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed|array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getOriginal',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6860fb1b6d83cb00432b01e7bf2cea18' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the model\'s original attribute values.
     *
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed|array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getOriginalWithoutRewindingModel',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5adc24868008164aaeb5eaa757584b63' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the model\'s raw original attribute values.
     *
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed|array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRawOriginal',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7ba35667eb6b0b25c30f2af1b89f7376' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a subset of the model\'s attributes.
     *
     * @param  array|mixed  $attributes
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'only',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6df2d88a470758753c3f1b2dee5e8baf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Sync the original attributes with the current.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'syncOriginal',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '468531ad79d280afe89b22b162afdf75' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Sync a single original attribute with its current value.
     *
     * @param  string  $attribute
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'syncOriginalAttribute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cafb1b3d92ea65f00417981f2dec8899' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Sync multiple original attribute with their current values.
     *
     * @param  array|string  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'syncOriginalAttributes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fbba998d976202562932d95dca87dead' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Sync the changed attributes.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'syncChanges',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0e9cab3d284b3017420612649a3d2739' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model or any of the given attribute(s) have been modified.
     *
     * @param  array|string|null  $attributes
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isDirty',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e3f29802ef6b5c7bb1c72d4bf307e852' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model or all the given attribute(s) have remained the same.
     *
     * @param  array|string|null  $attributes
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isClean',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '55e9cb93426c1abbc6b2f2e2bc2c8da9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model or any of the given attribute(s) have been modified.
     *
     * @param  array|string|null  $attributes
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'wasChanged',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cbfd5f2ef052bae2baf0d9a4df69c193' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if any of the given attributes were changed.
     *
     * @param  array  $changes
     * @param  array|string|null  $attributes
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasChanges',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba6cdb1261cec0e6652a6cadffdfee1e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the attributes that have been changed since the last sync.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getDirty',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'abed49f63b1578e7a4982a25bac11ba7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the attributes that were changed.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getChanges',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0651e39255e8739ffebad9cb8dd4af8b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the new and old values for a given key are equivalent.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'originalIsEquivalent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '65252ccc78ffe5b803b8dd87e1b5ae31' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Transform a raw model value using mutators, casts, etc.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'transformModelValue',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e849630bfcf2ef2f4188a326f29cad9b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Append attributes to query when building a query.
     *
     * @param  array|string  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'append',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '096d65c16e44cdbb609e62d09176d5a3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the accessors to append to model arrays.
     *
     * @param  array  $appends
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setAppends',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '86f030d0f13ac1717131913f290cb59f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Return whether the accessor attribute has been appended.
     *
     * @param  string  $attribute
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasAppended',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fa5d74208258af86478e274d571bc0fe' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the mutated attributes for a given instance.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getMutatedAttributes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dbfef28dc5538d8999b7be886d6c1373' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Extract and cache all the mutated attributes of a class.
     *
     * @param  string  $class
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'cacheMutatedAttributes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b101934634d96b7a16e7073320edcae7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all of the attribute mutator methods.
     *
     * @param  mixed  $class
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'carbonimmutable' => 'Carbon\\CarbonImmutable',
          'carboninterface' => 'Carbon\\CarbonInterface',
          'datetimeinterface' => 'DateTimeInterface',
          'castable' => 'Illuminate\\Contracts\\Database\\Eloquent\\Castable',
          'castsinboundattributes' => 'Illuminate\\Contracts\\Database\\Eloquent\\CastsInboundAttributes',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'asarrayobject' => 'Illuminate\\Database\\Eloquent\\Casts\\AsArrayObject',
          'ascollection' => 'Illuminate\\Database\\Eloquent\\Casts\\AsCollection',
          'invalidcastexception' => 'Illuminate\\Database\\Eloquent\\InvalidCastException',
          'jsonencodingexception' => 'Illuminate\\Database\\Eloquent\\JsonEncodingException',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'lazyloadingviolationexception' => 'Illuminate\\Database\\LazyLoadingViolationException',
          'arr' => 'Illuminate\\Support\\Arr',
          'carbon' => 'Illuminate\\Support\\Carbon',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'crypt' => 'Illuminate\\Support\\Facades\\Crypt',
          'date' => 'Illuminate\\Support\\Facades\\Date',
          'str' => 'Illuminate\\Support\\Str',
          'invalidargumentexception' => 'InvalidArgumentException',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getMutatorMethods',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4fff2c710d6d0722d68f180d96b4aca1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f314512ef3abd5128c3c85b39fed4419' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * User exposed observable events.
     *
     * These are extra user-defined events observers may subscribe to.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c30c1fd3945cf6bca56d5685e319b2ad' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register observers with the model.
     *
     * @param  object|array|string  $classes
     * @return void
     *
     * @throws \\RuntimeException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'observe',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd6cdd5290724684218214b613da12493' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a single observer with the model.
     *
     * @param  object|string  $class
     * @return void
     *
     * @throws \\RuntimeException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'registerObserver',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5234f8a5449a66c826da27997538e6b5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Resolve the observer\'s class name from an object or string.
     *
     * @param  object|string  $class
     * @return string
     *
     * @throws \\InvalidArgumentException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveObserverClassName',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1ed9fd06371a996e52326e2df29648b2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the observable event names.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getObservableEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3147d37540d1e48c98fa77490d60b5d3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the observable event names.
     *
     * @param  array  $observables
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setObservableEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0cad313751f82500f54913ce7b002d9a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Add an observable event name.
     *
     * @param  array|mixed  $observables
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'addObservableEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '897cca55831aee28bd4ad5f3625ee566' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Remove an observable event name.
     *
     * @param  array|mixed  $observables
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'removeObservableEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '65035604cf0490bb0293b9bf7f1f4d5a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a model event with the dispatcher.
     *
     * @param  string  $event
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'registerModelEvent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bf05bc588862a48e893c5daeda6c39a9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Fire the given event for the model.
     *
     * @param  string  $event
     * @param  bool  $halt
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fireModelEvent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1802f9bef1d5fad090cd1d069cadaff5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Fire a custom model event for the given event.
     *
     * @param  string  $event
     * @param  string  $method
     * @return mixed|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fireCustomModelEvent',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cc1ac2c976d1fb6be607d409f82de157' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Filter the model event results.
     *
     * @param  mixed  $result
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'filterModelEventResults',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '46499254cbd0a1ca6341ad66074ec53e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a retrieved model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'retrieved',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '73d2d8b8a2633ceed6cd3f9d45d7d26f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a saving model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'saving',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'abb5940a1c04cbca9dedd2d9c5ad21d6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a saved model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'saved',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fb0bfc7d8d38bebe41673272a12a6427' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register an updating model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'updating',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bd3acd101799e607a1858e62e5ca993c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register an updated model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'updated',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2457d5dbd5978e44d0cf118b487e6fbc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a creating model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'creating',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'af915958bce72df20e82da9966ba4e2c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a created model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'created',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e00ffdfdbf7d44ca2ecd142943e753b2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a replicating model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'replicating',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '66669507edfef7a1012429490a1ff41e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a deleting model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'deleting',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd9e18774869b9c61574f5aa10185235d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a deleted model event with the dispatcher.
     *
     * @param  \\Closure|string  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'deleted',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a30b54df5994cbd3aa5894925782f1d4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Remove all of the event listeners for the model.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'flushEventListeners',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '282d217b8bbe7d37f9d0724e7c1dbed4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the event dispatcher instance.
     *
     * @return \\Illuminate\\Contracts\\Events\\Dispatcher
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getEventDispatcher',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '35390034082008eeb27d9b61864d10a0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the event dispatcher instance.
     *
     * @param  \\Illuminate\\Contracts\\Events\\Dispatcher  $dispatcher
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setEventDispatcher',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1d515ef87da843d2a35ea27b0ad9bebd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset the event dispatcher for models.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'unsetEventDispatcher',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9f7a92aaff053d2e8c97195b63313cf8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Execute a callback without firing any model events for any model type.
     *
     * @param  callable  $callback
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'dispatcher' => 'Illuminate\\Contracts\\Events\\Dispatcher',
          'nulldispatcher' => 'Illuminate\\Events\\NullDispatcher',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'withoutEvents',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '31313c4f58d0af6c1a57f0b2ec8c1669' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a new global scope on the model.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Scope|\\Closure|string  $scope
     * @param  \\Closure|null  $implementation
     * @return mixed
     *
     * @throws \\InvalidArgumentException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'scope' => 'Illuminate\\Database\\Eloquent\\Scope',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'addGlobalScope',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dce33c72f06ca8760ad7009fc126eee8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if a model has a global scope.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Scope|string  $scope
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'scope' => 'Illuminate\\Database\\Eloquent\\Scope',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasGlobalScope',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '05c8fd9c75644c7390d53e3ff5e47e35' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a global scope registered with the model.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Scope|string  $scope
     * @return \\Illuminate\\Database\\Eloquent\\Scope|\\Closure|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'scope' => 'Illuminate\\Database\\Eloquent\\Scope',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getGlobalScope',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd0ded1a1fb6e3f29b478f18189a56ac8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the global scopes for this class instance.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'scope' => 'Illuminate\\Database\\Eloquent\\Scope',
          'arr' => 'Illuminate\\Support\\Arr',
          'invalidargumentexception' => 'InvalidArgumentException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getGlobalScopes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'af975c570876e55c40ebdcd71341b285' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The loaded relationships for the model.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cbc247e46fc319e283b614dedc16032a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The relationships that should be touched on save.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba85155a65aa565b4727a72f9103a08c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The many to many relationship methods.
     *
     * @var string[]
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4356e8540af9418fd780a656f06fe0c9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The relation resolver callbacks.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e512d88343cae7f12d545ea88daf83a5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a dynamic relation resolver.
     *
     * @param  string  $name
     * @param  \\Closure  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveRelationUsing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bd6bfc8e243ffdabd759a51f37fce125' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a one-to-one relationship.
     *
     * @param  string  $related
     * @param  string|null  $foreignKey
     * @param  string|null  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasOne
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasOne',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ef7581f7b6e9b666699103638b941020' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new HasOne relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $foreignKey
     * @param  string  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasOne
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newHasOne',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '89f929eb6f8d8c13dcc4b67712c85b59' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a has-one-through relationship.
     *
     * @param  string  $related
     * @param  string  $through
     * @param  string|null  $firstKey
     * @param  string|null  $secondKey
     * @param  string|null  $localKey
     * @param  string|null  $secondLocalKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasOneThrough',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8968a68a4604c995ea9e84b7225a38e8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new HasOneThrough relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $farParent
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $throughParent
     * @param  string  $firstKey
     * @param  string  $secondKey
     * @param  string  $localKey
     * @param  string  $secondLocalKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newHasOneThrough',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd0604c2c0ed91b7d26459716cbe4647a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic one-to-one relationship.
     *
     * @param  string  $related
     * @param  string  $name
     * @param  string|null  $type
     * @param  string|null  $id
     * @param  string|null  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphOne
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphOne',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '83de48870d539c318b8f5fafe328ac03' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new MorphOne relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $type
     * @param  string  $id
     * @param  string  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphOne
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newMorphOne',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f5b5917f02183b581f7705d20143464f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define an inverse one-to-one or many relationship.
     *
     * @param  string  $related
     * @param  string|null  $foreignKey
     * @param  string|null  $ownerKey
     * @param  string|null  $relation
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\BelongsTo
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'belongsTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'baaed676c4be332b244c73fb700ade79' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new BelongsTo relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $child
     * @param  string  $foreignKey
     * @param  string  $ownerKey
     * @param  string  $relation
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\BelongsTo
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newBelongsTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '586b7813ec1717c921d301c49eb8046e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic, inverse one-to-one or many relationship.
     *
     * @param  string|null  $name
     * @param  string|null  $type
     * @param  string|null  $id
     * @param  string|null  $ownerKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphTo
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '29748bb35c2a047019cc3175e01238c3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic, inverse one-to-one or many relationship.
     *
     * @param  string  $name
     * @param  string  $type
     * @param  string  $id
     * @param  string  $ownerKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphTo
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphEagerTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9857134ff2171d69d35e3b9301278540' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic, inverse one-to-one or many relationship.
     *
     * @param  string  $target
     * @param  string  $name
     * @param  string  $type
     * @param  string  $id
     * @param  string  $ownerKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphTo
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphInstanceTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae40b59c5a643a7cf30da46cb8fe8f59' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new MorphTo relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $foreignKey
     * @param  string  $ownerKey
     * @param  string  $type
     * @param  string  $relation
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphTo
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newMorphTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8529914d93076338d7a036008c2f4718' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Retrieve the actual class name for a given morph class.
     *
     * @param  string  $class
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getActualClassNameForMorph',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e08c83119cae005a8d1939955409a964' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Guess the "belongs to" relationship name.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'guessBelongsToRelation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cfe409e19acdc5dd10e89069948db904' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a one-to-many relationship.
     *
     * @param  string  $related
     * @param  string|null  $foreignKey
     * @param  string|null  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f66a7d49123c65be57f05300bd10bcdd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new HasMany relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $foreignKey
     * @param  string  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newHasMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '245077878e8b0b9871b1afb14f403968' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a has-many-through relationship.
     *
     * @param  string  $related
     * @param  string  $through
     * @param  string|null  $firstKey
     * @param  string|null  $secondKey
     * @param  string|null  $localKey
     * @param  string|null  $secondLocalKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasManyThrough',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd41843280f9e5b605dc970b16d6a7136' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new HasManyThrough relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $farParent
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $throughParent
     * @param  string  $firstKey
     * @param  string  $secondKey
     * @param  string  $localKey
     * @param  string  $secondLocalKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newHasManyThrough',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '472910111f6ada1a507a7e0cc567706a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic one-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $name
     * @param  string|null  $type
     * @param  string|null  $id
     * @param  string|null  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3bb1c800563f9086cf96627ee6bbf46d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new MorphMany relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $type
     * @param  string  $id
     * @param  string  $localKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newMorphMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '79fef97d0fed34f4d6dce391525bc9cf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a many-to-many relationship.
     *
     * @param  string  $related
     * @param  string|null  $table
     * @param  string|null  $foreignPivotKey
     * @param  string|null  $relatedPivotKey
     * @param  string|null  $parentKey
     * @param  string|null  $relatedKey
     * @param  string|null  $relation
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'belongsToMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bcad96616097e9f0c19c77f94d3650d9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new BelongsToMany relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $table
     * @param  string  $foreignPivotKey
     * @param  string  $relatedPivotKey
     * @param  string  $parentKey
     * @param  string  $relatedKey
     * @param  string|null  $relationName
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newBelongsToMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b4369650e26bd2ff6886072e0f1ef717' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic many-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $name
     * @param  string|null  $table
     * @param  string|null  $foreignPivotKey
     * @param  string|null  $relatedPivotKey
     * @param  string|null  $parentKey
     * @param  string|null  $relatedKey
     * @param  bool  $inverse
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphToMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphToMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a88f36699a22f16f770bf45749ea543d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Instantiate a new MorphToMany relationship.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  string  $name
     * @param  string  $table
     * @param  string  $foreignPivotKey
     * @param  string  $relatedPivotKey
     * @param  string  $parentKey
     * @param  string  $relatedKey
     * @param  string|null  $relationName
     * @param  bool  $inverse
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphToMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newMorphToMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b9efb7901edb935a6de7c115f88a9db6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Define a polymorphic, inverse many-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $name
     * @param  string|null  $table
     * @param  string|null  $foreignPivotKey
     * @param  string|null  $relatedPivotKey
     * @param  string|null  $parentKey
     * @param  string|null  $relatedKey
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\MorphToMany
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'morphedByMany',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '02c550c11dfa5880b61a61d9dd65d1ad' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the relationship name of the belongsToMany relationship.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'guessBelongsToManyRelation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2a9000e3f8c279a51ae5259e9c3706ef' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the joining table name for a many-to-many relation.
     *
     * @param  string  $related
     * @param  \\Illuminate\\Database\\Eloquent\\Model|null  $instance
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'joiningTable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6b3a2d4ec83507bafb9a56ba8f540eca' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get this model\'s half of the intermediate table name for belongsToMany relationships.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'joiningTableSegment',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '281b1fe12fe280e8907d88d46074428c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model touches a given relation.
     *
     * @param  string  $relation
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'touches',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f8fa0c1f85154c63b54658b8b01340c2' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Touch the owning relations of the model.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'touchOwners',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f683b9acbb3a289c8e581458fdcb9f25' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the polymorphic relationship columns.
     *
     * @param  string  $name
     * @param  string  $type
     * @param  string  $id
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getMorphs',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '00f175af44e0c5cd6590cc12fdf621cf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the class name for polymorphic relations.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getMorphClass',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4470f7c9bacaa7462efddb475532b682' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new model instance for a related model.
     *
     * @param  string  $class
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newRelatedInstance',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8892545c56a0096c398a1ff8486fa42b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all the loaded relations for the instance.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '058fe155c70b73a6212e461e833ee206' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a specified relationship.
     *
     * @param  string  $relation
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRelation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c8bfef37737a5c07395d40d012fce522' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given relation is loaded.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'relationLoaded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7978968f588b5a55f494a83bc1545369' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the given relationship on the model.
     *
     * @param  string  $relation
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setRelation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd5dd14a31e8be23ec03937c34f5bd725' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset a loaded relationship.
     *
     * @param  string  $relation
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'unsetRelation',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '373a43d501aa3d9941d04044aa6ad2c4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the entire relations array on the model.
     *
     * @param  array  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '49c57b86a0507572cd9eca89605815d1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Duplicate the instance and unset all the loaded relations.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'withoutRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ec597b30927ea77008d01fa7192e580f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset all the loaded relations for the instance.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'unsetRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8037e077838148e0f3975b7216f1f70c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the relationships that are touched on save.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getTouchedRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '51fb25a37b8f04a08f782d173ac99c07' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the relationships that are touched on save.
     *
     * @param  array  $touches
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
          'classmorphviolationexception' => 'Illuminate\\Database\\ClassMorphViolationException',
          'builder' => 'Illuminate\\Database\\Eloquent\\Builder',
          'collection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'model' => 'Illuminate\\Database\\Eloquent\\Model',
          'belongsto' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsTo',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'hasmany' => 'Illuminate\\Database\\Eloquent\\Relations\\HasMany',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'hasone' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOne',
          'hasonethrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasOneThrough',
          'morphmany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphMany',
          'morphone' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphOne',
          'morphto' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphTo',
          'morphtomany' => 'Illuminate\\Database\\Eloquent\\Relations\\MorphToMany',
          'relation' => 'Illuminate\\Database\\Eloquent\\Relations\\Relation',
          'arr' => 'Illuminate\\Support\\Arr',
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setTouchedRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f545014b0f9d6ce6398263becf62bc71' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0f0caecce0ba825c322b1cc4694e039c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Update the model\'s update timestamp.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'touch',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '77e419f37db97d4b3e6aa4287d45951c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Update the creation and update timestamps.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'updateTimestamps',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c7435831111344715c363a7a08cffc83' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the value of the "created at" attribute.
     *
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setCreatedAt',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'cfc449dcf257a52ea2e74c6cedc6be0a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the value of the "updated at" attribute.
     *
     * @param  mixed  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setUpdatedAt',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'fe738bb3a17c66339717ef224a60f2bd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a fresh timestamp for the model.
     *
     * @return \\Illuminate\\Support\\Carbon
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'freshTimestamp',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0246a8371b98ff520ced81f60b9c7311' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a fresh timestamp for the model.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'freshTimestampString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c6985614c29edd4b1117183c7ce4e83c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model uses timestamps.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'usesTimestamps',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b3ae7e7c98a136ad235639f567712ebd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the name of the "created at" column.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getCreatedAtColumn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0e51e17e414a0db6cac5bb5a3d9c33de' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the name of the "updated at" column.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getUpdatedAtColumn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd13cbd35127303252b3e84248c4096a1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the fully qualified "created at" column.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getQualifiedCreatedAtColumn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2940a3163239873a20885951f4fa0b89' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the fully qualified "updated at" column.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'date' => 'Illuminate\\Support\\Facades\\Date',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getQualifiedUpdatedAtColumn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2c9d4fc97a2f31f8b48fb3567184e4fa' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5761946e185069b0da664b3bcdc71e16' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ff4ceb2525339236144decd333cfef25' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the hidden attributes for the model.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getHidden',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e5fd47f5ae8e537983d301c1ea761ef5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the hidden attributes for the model.
     *
     * @param  array  $hidden
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setHidden',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e0f06872953cae55334130b438b38106' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the visible attributes for the model.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getVisible',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f1b5033fc65a93490633876f1f5d4a28' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the visible attributes for the model.
     *
     * @param  array  $visible
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setVisible',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3ccec484da526ec7193b6e417477ff95' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Make the given, typically hidden, attributes visible.
     *
     * @param  array|string|null  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'makeVisible',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '854a0d58a138c1ad473624ed17122e96' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Make the given, typically hidden, attributes visible if the given truth test passes.
     *
     * @param  bool|Closure  $condition
     * @param  array|string|null  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'makeVisibleIf',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '763dd50cc99aca052d04b47d816a4955' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Make the given, typically visible, attributes hidden.
     *
     * @param  array|string|null  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'makeHidden',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7dd9c68bb2b2dbba899a394706fa0aa1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Make the given, typically visible, attributes hidden if the given truth test passes.
     *
     * @param  bool|Closure  $condition
     * @param  array|string|null  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'closure' => 'Closure',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'makeHiddenIf',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1c177501452789530176abf4df0f3753' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '654c8753e645872afcc1b3918c93fd0b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The attributes that aren\'t mass assignable.
     *
     * @var string[]|bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b103349ff5a38ff3903fd42c44fb9328' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b4ae9d991f6ae399f2537f3e83c3999e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The actual columns that exist on the database and can be guarded.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'abf5cc5cf7c06801fc5c3dfc4652ea72' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the fillable attributes for the model.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getFillable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e8f3da2316ef4d06f297d43f877b596e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the fillable attributes for the model.
     *
     * @param  array  $fillable
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fillable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '19e9d90c6d252e5db4945ecfced9100d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Merge new fillable attributes with existing fillable attributes on the model.
     *
     * @param  array  $fillable
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'mergeFillable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '019db74465fbe444218be2eec7afe7ac' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the guarded attributes for the model.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getGuarded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1c7ea7408a4d1676bc1e0a5bff4cea39' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the guarded attributes for the model.
     *
     * @param  array  $guarded
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'guard',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '70a664e7f952eb885620addad2677324' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Merge new guarded attributes with existing guarded attributes on the model.
     *
     * @param  array  $guarded
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'mergeGuarded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b5dc11ab1f4116d38be66989b6f574e1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Disable all mass assignable restrictions.
     *
     * @param  bool  $state
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'unguard',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8f27f19f6dfed88d2727f7fe87d92ef8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Enable the mass assignment restrictions.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'reguard',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3cbc35fe379450ef8887d124cb1f6323' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the current state is "unguarded".
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isUnguarded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8b23cc3ce425925da632525ce1649adc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Run the given callable while being unguarded.
     *
     * @param  callable  $callback
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'unguarded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3a4f5564ac05261b995cdbb78fd8e36b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given attribute may be mass assigned.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isFillable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a28d895cb24b6b2b45e13e624983f51e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given key is guarded.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isGuarded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5db37ff4ead0f92d549e2054ec4cb4a5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given column is a valid, guardable column.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isGuardableColumn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1cb9808d869274a657edc72016896880' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model is totally guarded.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'totallyGuarded',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a0cb9d485982501d3dc1905b64e43a19' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the fillable attributes of a given array.
     *
     * @param  array  $attributes
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent\\Concerns',
         'uses' => 
        array (
          'str' => 'Illuminate\\Support\\Str',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fillableFromArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1b099b7b65fbe1983dfa61429ebcd824' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Forward a method call to the given object.
     *
     * @param  mixed  $object
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'error' => 'Error',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'forwardCallTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '058d869634bc2732b48a669e8dc5b254' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Forward a method call to the given object, returning $this if the forwarded call returned itself.
     *
     * @param  mixed  $object
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'error' => 'Error',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'forwardDecoratedCallTo',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8adb89239cd29e12e0c7557b949a597d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Throw a bad method call exception for the given method.
     *
     * @param  string  $method
     * @return void
     *
     * @throws \\BadMethodCallException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Support\\Traits',
         'uses' => 
        array (
          'badmethodcallexception' => 'BadMethodCallException',
          'error' => 'Error',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'throwBadMethodCallException',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b3fae64b714b3382002a541ce94ff76e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The connection name for the model.
     *
     * @var string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3b83d891feb4f72d97cb8ff04e72dd75' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The table associated with the model.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e133ca116e6c1989daae0adf47faf718' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The primary key for the model.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd85216895a33daa089faace56d2868b3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The "type" of the primary key ID.
     *
     * @var string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9a5541b2551af33231da8db12e667694' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8aa76968031401363d8bdfa8347d221d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The relations to eager load on every query.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b806a1a0b934806131b6038d2e576e28' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The relationship counts that should be eager loaded on every query.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7c9e339d35535e96e93d6592b5a99dbd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates whether lazy loading will be prevented on this model.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '08ead7bc6df8920d29f1bf3703cc0f17' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The number of models to return for pagination.
     *
     * @var int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1f03c7f4bfded831ca33cabc657b80b6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if the model exists.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'be1419c066ab605e16767b7548a57a44' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if the model was inserted during the current request lifecycle.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9c0a7d1c5e15025767a57eeca2e6a453' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The connection resolver instance.
     *
     * @var \\Illuminate\\Database\\ConnectionResolverInterface
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5aa0047afe27b7b61ad77680a27cff64' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The event dispatcher instance.
     *
     * @var \\Illuminate\\Contracts\\Events\\Dispatcher
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2adb820b05cf6cea72b6235b49d9de55' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The array of booted models.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e5efb52f9d427591d8cc64e11eaa2816' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The array of trait initializers that will be called on each new instance.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '230798b064cb0867f2b00e45783055a0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The array of global scopes on the model.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ad7a2bfeaeee973ba129a3e30507ea87' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The list of models classes that should not be affected with touch.
     *
     * @var array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ca0f71a961a1f9d9513f959516a40d46' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates whether lazy loading should be restricted on all models.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0725d64c6f00587bf12e8758cead2b28' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The callback that is responsible for handling lazy loading violations.
     *
     * @var callable|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c57eb27fdc99ee91611436170faaf1f5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Indicates if broadcasting is currently enabled.
     *
     * @var bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c28230c47a0e3556a0c73358c52ca7c6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The name of the "created at" column.
     *
     * @var string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c6f7aa5fb1cc4e6646f2a7e4aa862652' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * The name of the "updated at" column.
     *
     * @var string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => NULL,
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a1565bfe82d8d628c49db46d4423ca56' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__construct',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3f2d746287b59590631bb578bccfa86d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Check if the model needs to be booted and if so, do it.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'bootIfNotBooted',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4d9ff9660656d4375aad831fa456453f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Perform any actions required before the model boots.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'booting',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c820780ee35fe14bc3033d28ad05ae62' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Bootstrap the model and its traits.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'boot',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'bc45cac4bbdc36833477167ad681cd96' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Boot all of the bootable traits on the model.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'bootTraits',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae1263fd36fd006ff31412f1205ff8d1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Initialize any initializable traits on the model.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'initializeTraits',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '331340822d93933a0a9592f6fa9cc317' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Perform any actions required after the model boots.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'booted',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '18d7bffaa531c00216a4b29167e160a8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Clear the list of booted models so they will be re-booted.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'clearBootedModels',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '56636097a3854a1d4a8ae91bb93d789f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Disables relationship model touching for the current class during given callback scope.
     *
     * @param  callable  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'withoutTouching',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '535b80870e776b79943df015a27df06c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Disables relationship model touching for the given model classes during given callback scope.
     *
     * @param  array  $models
     * @param  callable  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'withoutTouchingOn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a9d484b3b131242eeca42d9a05a40152' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given model is ignoring touches.
     *
     * @param  string|null  $class
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isIgnoringTouch',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '83ea1df480c3b863e896a7a1b4baf235' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Prevent model relationships from being lazy loaded.
     *
     * @param  bool  $value
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'preventLazyLoading',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '277ef5b72b9ede734badfb16fd7cf7e5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register a callback that is responsible for handling lazy loading violations.
     *
     * @param  callable  $callback
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'handleLazyLoadingViolationUsing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '00a5cd495d790a7cb6c774c6d9eb0e66' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Execute a callback without broadcasting any model events for all model types.
     *
     * @param  callable  $callback
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'withoutBroadcasting',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dbccdeaebf4e533f4675e35c77c7c043' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \\Illuminate\\Database\\Eloquent\\MassAssignmentException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fill',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4fa90bc9420bff48e0a6b99ff099f389' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Fill the model with an array of attributes. Force mass assignment.
     *
     * @param  array  $attributes
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'forceFill',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '36506ac4d2cc3801576f3c28c21ac6dc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Qualify the given column name by the model\'s table.
     *
     * @param  string  $column
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'qualifyColumn',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '408e2dc71f43569b979c4ca5e585af01' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Qualify the given columns with the model\'s table.
     *
     * @param  array  $columns
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'qualifyColumns',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '22b7dfacd79db19ccf01e3aa363b6f01' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new instance of the given model.
     *
     * @param  array  $attributes
     * @param  bool  $exists
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newInstance',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '97dd80f72a5f19952d838809d9fd6c73' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new model instance that is existing.
     *
     * @param  array  $attributes
     * @param  string|null  $connection
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newFromBuilder',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1baeb68f62cfa9da67c048c31a4bca06' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Begin querying the model on a given connection.
     *
     * @param  string|null  $connection
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'on',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c722f35c0f718c594f5fb625ee07f21a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Begin querying the model on the write connection.
     *
     * @return \\Illuminate\\Database\\Query\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'onWriteConnection',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '821e4911b45bd114915a9591353a5543' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get all of the models from the database.
     *
     * @param  array|mixed  $columns
     * @return \\Illuminate\\Database\\Eloquent\\Collection|static[]
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'all',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '5323f0477fad98d297e89e0773442487' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Begin querying a model with eager loading.
     *
     * @param  array|string  $relations
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'with',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '24441fdd8b68816285a15636a88b9319' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relations on the model.
     *
     * @param  array|string  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'load',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '271a80daf09b7c09d99cbfb62dfe951d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationships on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorph',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1bc16956abf996e46ba46432a7e15716' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relations on the model if they are not already eager loaded.
     *
     * @param  array|string  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMissing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4b72f9fdb762cd47bf7bcd48f5db17a8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relation\'s column aggregations on the model.
     *
     * @param  array|string  $relations
     * @param  string  $column
     * @param  string  $function
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadAggregate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0be790795118d5df61b8ada08b71b1b1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relation counts on the model.
     *
     * @param  array|string  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '787239879e8326197bcdfdbe2c067c03' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relation max column values on the model.
     *
     * @param  array|string  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMax',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '0703610e096a21bf35889cebb5679a7e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relation min column values on the model.
     *
     * @param  array|string  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMin',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '919b5368de7520bfe5002874479fb442' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relation\'s column summations on the model.
     *
     * @param  array|string  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadSum',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4ffc61274a4d9e6aa57e4bda3c3b6ee4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relation average column values on the model.
     *
     * @param  array|string  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadAvg',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b041323ebcbd3f6d5d78ae6184a3a2c5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load related model existence values on the model.
     *
     * @param  array|string  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ce605663a81518d129de81ab3e59d143' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationship column aggregation on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @param  string  $column
     * @param  string  $function
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorphAggregate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dfd40349bd1c3321144ec7fc8d69deff' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationship counts on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorphCount',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a36709ec3b2c2bf66aabd3f6356c5501' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationship max column values on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorphMax',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9d96550cd67cdb56f20971af700dc744' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationship min column values on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorphMin',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ef4e0c34e32986f309ed4d6f5ab30ecc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationship column summations on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorphSum',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba3cfb24acee7731816645f24f1fe0bc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Eager load relationship average column values on the polymorphic relation of a model.
     *
     * @param  string  $relation
     * @param  array  $relations
     * @param  string  $column
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'loadMorphAvg',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '558ec9b9716375a03e49868024f54a15' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Increment a column\'s value by a given amount.
     *
     * @param  string  $column
     * @param  float|int  $amount
     * @param  array  $extra
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'increment',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3f91fe2d57ee016cfad01c14a81d804c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Decrement a column\'s value by a given amount.
     *
     * @param  string  $column
     * @param  float|int  $amount
     * @param  array  $extra
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'decrement',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4c49b40b7400d230bbad1abd609fdd33' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Run the increment or decrement method on the model.
     *
     * @param  string  $column
     * @param  float|int  $amount
     * @param  array  $extra
     * @param  string  $method
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'incrementOrDecrement',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ab7bec305283ac622067a5e4052e878e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Update the model in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'update',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '07811dae74825e287923264d44a9b290' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Update the model in the database within a transaction.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     *
     * @throws \\Throwable
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'updateOrFail',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6d9b988071a9b533e31fbcf8a79f9c97' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Update the model in the database without raising any events.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'updateQuietly',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '946354194739eb96368973857797c2cc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Save the model and all of its relationships.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'push',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '41ece5ce2a042a936b8df65e780444d1' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Save the model to the database without raising any events.
     *
     * @param  array  $options
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'saveQuietly',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7f9861538f4df16519dc1976568fe11e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'save',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '695d2bf7a57229f1eb022349c6308bf4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Save the model to the database within a transaction.
     *
     * @param  array  $options
     * @return bool
     *
     * @throws \\Throwable
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'saveOrFail',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2d76f960dbb08902f7bc9c85e5fc652c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Perform any actions that are necessary after the model is saved.
     *
     * @param  array  $options
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'finishSave',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '87399b5a81aca503f74c3c0a34cae7bb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Perform a model update operation.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'performUpdate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a80574e074d151dbce1ad337f2a8c6e5' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the keys for a select query.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setKeysForSelectQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9c40b5978f3a41f56e5c0088a218f4f6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the primary key value for a select query.
     *
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getKeyForSelectQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '771da0f1197759b232662d27e21cbbcb' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the keys for a save update query.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setKeysForSaveQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '52cbf8ba647777504182f6d5e0df286f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the primary key value for a save query.
     *
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getKeyForSaveQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '992024d016082f005b9ace4271a4555c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Perform a model insert operation.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'performInsert',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'da3c845a45651d2044ac19f47d33fc43' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Insert the given attributes and set the ID on the model.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $query
     * @param  array  $attributes
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'insertAndSetId',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '82d3967a475385871d7103102c62badc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Destroy the models for the given IDs.
     *
     * @param  \\Illuminate\\Support\\Collection|array|int|string  $ids
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'destroy',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae36995de8951b9de38b357a43e98c6f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws \\LogicException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'delete',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '14969c5f859766513ee732a2ca7edbbd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Delete the model from the database within a transaction.
     *
     * @return bool|null
     *
     * @throws \\Throwable
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'deleteOrFail',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8dca4330937f01b7188ee9412fd417bc' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Force a hard delete on a soft deleted model.
     *
     * This method protects developers from running forceDelete when the trait is missing.
     *
     * @return bool|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'forceDelete',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '43ce9615c24812f69c02d4392c32209d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Perform the actual delete query on this model instance.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'performDeleteOnModel',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7e8500e89dcb1f64382b198f0457f570' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Begin querying the model.
     *
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'query',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e8e4caefe16e8293e0458673c6eb39dd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query builder for the model\'s table.
     *
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4f6cf83e59fad3fdbb8478a1696ffb4c' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query builder that doesn\'t have any global scopes or eager loading.
     *
     * @return \\Illuminate\\Database\\Eloquent\\Builder|static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newModelQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '8cde644a12990596a8d8e1ce4b1f09b0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query builder with no relationships loaded.
     *
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newQueryWithoutRelationships',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f2fc8a8aa22321780ef10de09bbd3f53' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Register the global scopes for this builder instance.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Builder  $builder
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'registerGlobalScopes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '09d0a17f8a13dba81fbdf86adc8f16c4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query builder that doesn\'t have any global scopes.
     *
     * @return \\Illuminate\\Database\\Eloquent\\Builder|static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newQueryWithoutScopes',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c99a36a901b28ec2149b9172a6d881c9' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query instance without a given scope.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Scope|string  $scope
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newQueryWithoutScope',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'aa217bc17483009f5c032ec961c948ac' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query to restore one or more models by their queueable IDs.
     *
     * @param  array|int  $ids
     * @return \\Illuminate\\Database\\Eloquent\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newQueryForRestoration',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1d6bd2393e7ad06203e59b0ef59b60ff' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \\Illuminate\\Database\\Query\\Builder  $query
     * @return \\Illuminate\\Database\\Eloquent\\Builder|static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newEloquentBuilder',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'aa6f3c3141b829ef2398d4b2f5f0d885' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get a new query builder instance for the connection.
     *
     * @return \\Illuminate\\Database\\Query\\Builder
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newBaseQueryBuilder',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c21cdf409db0c4ad2052862b0980b270' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \\Illuminate\\Database\\Eloquent\\Collection
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newCollection',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ca4afed252206b8086e5add23726c75d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Create a new pivot model instance.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Model  $parent
     * @param  array  $attributes
     * @param  string  $table
     * @param  bool  $exists
     * @param  string|null  $using
     * @return \\Illuminate\\Database\\Eloquent\\Relations\\Pivot
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'newPivot',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ff3f03600fefdb2b3c1281e0e67c51bf' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the model has a given scope.
     *
     * @param  string  $scope
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'hasNamedScope',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c1bad6cad53afb68de9e1ebb16c904f4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Apply the given named scope if possible.
     *
     * @param  string  $scope
     * @param  array  $parameters
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'callNamedScope',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '357a72dd329223c4da41c213c230d96a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Convert the model instance to an array.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'toArray',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '899317933943b4f0ea1073c7d35f6258' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     *
     * @throws \\Illuminate\\Database\\Eloquent\\JsonEncodingException
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'toJson',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '44653e6a5c4d857a0e44db32dd8eb83b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'jsonSerialize',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '683af7e4ff7325cd949ff131558c0b2b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Reload a fresh model instance from the database.
     *
     * @param  array|string  $with
     * @return static|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'fresh',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f2cbdd2e6a3fbfc42ef1029f57c57ec6' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Reload the current model instance with fresh attributes from the database.
     *
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'refresh',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '447778c55908c8e14999af5848f29899' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Clone the model into a new, non-existing instance.
     *
     * @param  array|null  $except
     * @return static
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'replicate',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3135516d93325c59d07a74a196ba5db3' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if two models have the same ID and belong to the same table.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Model|null  $model
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'is',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6792bc2b0f4880df0e832ee0b491deb0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if two models are not the same.
     *
     * @param  \\Illuminate\\Database\\Eloquent\\Model|null  $model
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'isNot',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '723425773ff86a2132b347381422d57d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the database connection for the model.
     *
     * @return \\Illuminate\\Database\\Connection
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getConnection',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f97cf49144b88b1ef7cc51d0264a88bd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the current connection name for the model.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getConnectionName',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c9cc116043aff9512800c33f213d22db' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the connection associated with the model.
     *
     * @param  string|null  $name
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setConnection',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7ecab22c59c8c8ca4060863590ce9588' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Resolve a connection instance.
     *
     * @param  string|null  $connection
     * @return \\Illuminate\\Database\\Connection
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveConnection',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7a88e0c17b62da9b51253f0e8efaa1a4' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the connection resolver instance.
     *
     * @return \\Illuminate\\Database\\ConnectionResolverInterface
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getConnectionResolver',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '7b0c758f0c854c860b5745e7985680ee' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the connection resolver instance.
     *
     * @param  \\Illuminate\\Database\\ConnectionResolverInterface  $resolver
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setConnectionResolver',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '612f170087bb4df6448a20d081a60dbd' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset the connection resolver for models.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'unsetConnectionResolver',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2c6b310512b9da734e7eb95940de62ed' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the table associated with the model.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getTable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dda0577114e19eaa9c6bfb433f50484f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the table associated with the model.
     *
     * @param  string  $table
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setTable',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b752e6d1ce0628bd65341b79cca0200a' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the primary key for the model.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getKeyName',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ed6aca501c30a30c808aa02200d79c3f' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the primary key for the model.
     *
     * @param  string  $key
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setKeyName',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '4baf5aa9062d49ea48622529747ed401' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the table qualified key name.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getQualifiedKeyName',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dc75e8f4ff97deb47a2201cee2d210df' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the auto-incrementing key type.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getKeyType',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e958529e3cf7e84d357a1c89337b4eb8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the data type for the primary key.
     *
     * @param  string  $type
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setKeyType',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '852a82aa047be6a6067f81aae5f53011' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getIncrementing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '28fad7cdb19374ad8ad541af3c6e814e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set whether IDs are incrementing.
     *
     * @param  bool  $value
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setIncrementing',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '3274c403e99badabf6ed38c26e8481f7' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value of the model\'s primary key.
     *
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd7e06abd1f4e9262734589b155367d9e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the queueable identity for the entity.
     *
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getQueueableId',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '9c2123732b905eea8840bd06eb896510' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the queueable relationships for the entity.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getQueueableRelations',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e59419a5b0ec3defc2ebff6c167978ba' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the queueable connection for the entity.
     *
     * @return string|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getQueueableConnection',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2002d446c2504d4d83c5081134a25bba' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value of the model\'s route key.
     *
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRouteKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '68658104a5fcb433fc50a25d3cabd88d' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the route key for the model.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getRouteKeyName',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c0fabf84230728645219feb02097dff8' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \\Illuminate\\Database\\Eloquent\\Model|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveRouteBinding',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '46c7964658626ae70e065cb5c9c9c682' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \\Illuminate\\Database\\Eloquent\\Model|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveSoftDeletableRouteBinding',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '88d3ab6a08617e544ddeac03a0e72021' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Retrieve the child model for a bound value.
     *
     * @param  string  $childType
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \\Illuminate\\Database\\Eloquent\\Model|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveChildRouteBinding',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'b39813b206d437c19da04ae8eca2fe07' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Retrieve the child model for a bound value.
     *
     * @param  string  $childType
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \\Illuminate\\Database\\Eloquent\\Model|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveSoftDeletableChildRouteBinding',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '432f626392e83251c562dd858f8eab04' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Retrieve the child model query for a bound value.
     *
     * @param  string  $childType
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \\Illuminate\\Database\\Eloquent\\Model|null
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'resolveChildRouteBindingQuery',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c0874cc011c2f8693de93de3171d1f92' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the default foreign key name for the model.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getForeignKey',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'a21bd3a48011e6627548add7a7a49b70' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the number of models to return per page.
     *
     * @return int
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'getPerPage',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c4cdd7e0d514b5288b249036cb18b01e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the number of models to return per page.
     *
     * @param  int  $perPage
     * @return $this
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'setPerPage',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '00b9c5b48eff7be34ba22d61c0e8894b' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if lazy loading is disabled.
     *
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'preventsLazyLoading',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'dce913b579df0bca69ec3126b02bb078' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the broadcast channel route definition that is associated with the given entity.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'broadcastChannelRoute',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'c238fbd661568909622b8cdd1321bf71' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the broadcast channel name that is associated with the given entity.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'broadcastChannel',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '798848f04657cd589c0326d8505266f0' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__get',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f14e6a9ff38b2a6e65633d9764195833' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__set',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '6b04805b4bbe8e1d72cbe1c9184b1a84' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'offsetExists',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'e2b31df1ffbcca88440b0c4b90ed2d04' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'offsetGet',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '79f817a70e4b34b752f304f2d99f1b9e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'offsetSet',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '04e93da8eebd6037b8b2bc23fa330e35' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => 'offsetUnset',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '2185702cc65b9f7ace0941b7ac68fa46' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__isset',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ba5d88f40a3ec94f75697e9bca9bcb65' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__unset',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'ae3c7b13c2ec280e524fd81a3f3bf6db' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__call',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f07879d1f8acc17ec4ac255bdc9e7521' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Handle dynamic static method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__callStatic',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'f16095d45e6fb801aff10d9f6073ac6e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Convert the model to its string representation.
     *
     * @return string
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__toString',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    '1419c01420ea71bd5d10db5b6494ee2e' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * Prepare the object for serialization.
     *
     * @return array
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__sleep',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
    'd346d16dc73511ee4f9e7cb878232cde' => 
    PHPStan\PhpDoc\NameScopedPhpDocString::__set_state(array(
       'phpDocString' => '/**
     * When a model is being unserialized, check if it needs to be booted.
     *
     * @return void
     */',
       'nameScope' => 
      PHPStan\Analyser\NameScope::__set_state(array(
         'namespace' => 'Illuminate\\Database\\Eloquent',
         'uses' => 
        array (
          'arrayaccess' => 'ArrayAccess',
          'hasbroadcastchannel' => 'Illuminate\\Contracts\\Broadcasting\\HasBroadcastChannel',
          'queueablecollection' => 'Illuminate\\Contracts\\Queue\\QueueableCollection',
          'queueableentity' => 'Illuminate\\Contracts\\Queue\\QueueableEntity',
          'urlroutable' => 'Illuminate\\Contracts\\Routing\\UrlRoutable',
          'arrayable' => 'Illuminate\\Contracts\\Support\\Arrayable',
          'jsonable' => 'Illuminate\\Contracts\\Support\\Jsonable',
          'resolver' => 'Illuminate\\Database\\ConnectionResolverInterface',
          'eloquentcollection' => 'Illuminate\\Database\\Eloquent\\Collection',
          'belongstomany' => 'Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany',
          'aspivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Concerns\\AsPivot',
          'hasmanythrough' => 'Illuminate\\Database\\Eloquent\\Relations\\HasManyThrough',
          'pivot' => 'Illuminate\\Database\\Eloquent\\Relations\\Pivot',
          'arr' => 'Illuminate\\Support\\Arr',
          'basecollection' => 'Illuminate\\Support\\Collection',
          'str' => 'Illuminate\\Support\\Str',
          'forwardscalls' => 'Illuminate\\Support\\Traits\\ForwardsCalls',
          'jsonserializable' => 'JsonSerializable',
          'logicexception' => 'LogicException',
        ),
         'className' => 'Illuminate\\Database\\Eloquent\\Model',
         'functionName' => '__wakeup',
         'templateTypeMap' => 
        PHPStan\Type\Generic\TemplateTypeMap::__set_state(array(
           'types' => 
          array (
          ),
           'lowerBoundTypes' => 
          array (
          ),
        )),
         'typeAliasesMap' => 
        array (
        ),
         'bypassTypeAliases' => false,
      )),
    )),
  ),
));