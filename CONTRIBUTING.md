
## Comment puis-je contribuer?

### Signaler des bogues

Cette section vous guide dans la soumission d'un rapport de bogue. Suivre ces directives aide les responsables et la communauté à comprendre votre rapport, reproduire le comportement, et trouver des rapports associés.

Lorsque vous créez un rapport de bogue, veuillez [inclure autant de détails que possible](#how-do-i-submit-a-good-bug-report). ,  les informations qu'il demande nous aident à résoudre les problèmes plus rapidement.

#### Comment soumettre un (bon)rapport de bogue

Les bogues sont suivis en tant que [GitHub issues](https://guides.github.com/features/issues/). Créez un problème sur ce référentiel et fournissez les informations correcte .

Expliquez le problème et incluez des détails supplémentaires pour aider les responsables à reproduire le problème :

* **Utilisez un titre clair et descriptif** pour le problème afin d'identifier le problème.
* **Décrivez les étapes exactes qui reproduisent le problème** iavec autant de détails que possible. 
* **Fournissez des exemples spécifiques pour illustrer les étapes**. Incluez des liens vers des fichiers ou des projets GitHub, ou des extraits de code à copier/coller, que vous utilisez dans ces exemples. Si vous fournissez des extraits dans le problème, utilisez des blocs de  [code Markdown](https://help.github.com/articles/markdown-basics/#multiple-lines).
* **Décrivez le comportement que vous avez observé après avoir suivi les étapes** et indiquez quel est exactement le problème avec ce comportement.
* **Expliquez quel comportement vous vous attendiez à voir à la place et pourquoi.**
* **Incluez des captures d'écran et des GIF animés** qui vous montrent suivre les étapes décrites et démontrent clairement le problème. 

### Suggérer des améliorations 

Cette section c'est pour envoyé les  suggestion d'amélioration pour notre projet, y compris de toutes nouvelles fonctionnalités et des améliorations mineures des fonctionnalités existantes. Suivre ces directives aide les responsables et la communauté à comprendre votre suggestion, et trouver des suggestions connexes.

Veuillez vous envoyer vous suggestion d'amélioration directement vers les GitLab qu'est mentionné sur le readme de notre projet 


